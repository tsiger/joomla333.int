<?php
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if(!defined('DS')){
	define('DS',DIRECTORY_SEPARATOR);
}

require_once( JPATH_COMPONENT.DS.'controller.php' );
require_once( JPATH_COMPONENT.DS.'billing15.php' );
require_once(JPATH_ROOT . '/components/com_billing/useraccount.php');

// Create the controller
if(JoomlaVersion() > '2.5')
{
	$classname	= 'BillingController';
}
else
{
	$classname	= 'BillingController';
}
$controller	= new $classname();

// Perform the Request task
$controller->execute( JRequest::getVar( 'task' ) );

// Redirect if set by the controller
$controller->redirect();