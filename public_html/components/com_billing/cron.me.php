<?php

define( '_JEXEC', 1 );

define('DS', DIRECTORY_SEPARATOR);
define('JPATH_BASE', substr(__FILE__,0,strrpos(__FILE__, DS."components")));

require_once( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once( JPATH_BASE .DS.'includes'.DS.'framework.php' );

$mainframe = JFactory::getApplication('site');
$mainframe->initialise();

include_once (JPATH_BASE .DS.'components'.DS.'com_billing'.DS.'useraccount.php');
include_once (JPATH_BASE .DS.'components'.DS.'com_billing'.DS.'userpoints.php');
include_once (JPATH_BASE .DS.'components'.DS.'com_billing'.DS.'account.php');

$lang = JFactory::getLanguage();
$lang->load('com_billing');

function JoomlaVersionCron()
{
	$version = new JVersion;
    $joomla = $version->getShortVersion();
    return substr($joomla,0,3);
}

function EmailToUser($message, $uid)
{

	$message .= '<br><br>' . JText::_('COM_BILLING_AUTO_MESSAGE');
	
	$subject = JText::_('COM_BILLING_SUB_NOTYF');
	$body = $message;
	$mode = 1;
	
	$user = JFactory::getUser($uid);
	$recipient = $user->email;
	
	$db = JFactory::getDBO();
	$query = "select * from `#__users` where username = 'admin' limit 0, 1";
	$result = $db->setQuery($query);   
	$row = $db->loadAssoc(); 
	$aname = $row['name'];
	$aemail = $row['email']; 

	$from = $aemail;
	$fromname = $aname;

	if (JoomlaVersionCron() == '1.5')
	{
		JUtility::sendMail($from, $fromname, $recipient, $subject, $body, $mode);
	}
	else
	{
		JFactory::getMailer()->sendMail($from, $fromname, $recipient, $subject, $body, $mode);
	}
	 
	return;
}

function GetSubscriptionTemplate($sid)
{
	$db = JFactory::getDBO();	
	$query = "select * from `#__billing_subscription_type` where id = '$sid'";
	$result = $db->setQuery($query);   
    $row = $db->loadAssoc();
	return $row['alarm_text'];
}

function GetSubscriptionName($sid)
{
	$db = JFactory::getDBO();	
	$query = "select * from `#__billing_subscription_type` where id = '$sid'";
	$result = $db->setQuery($query);   
    $row = $db->loadAssoc();
	return $row['subscription_type'];
}

function NotifyCronX($days, $message, $sid)
{
	$db = JFactory::getDBO();	
	
	$nowdate = date("Y-m-d");	
	$unixdate = strtotime($nowdate);
	$newdate = $unixdate + ($days)*86400;
	$newdate = date("Y-m-d", $newdate);
	

	$query = "select * from `#__billing_subscription` where enddate = '$newdate' and subscription_id = $sid and frozen = 0";
	$result = $db->setQuery($query);   
    $rows = $db->loadObjectList();
	if(count($rows) > 0)
	{
		foreach ($rows as $row)
		{
			$uid = $row->uid;

			if (!IsAlreadySend($nowdate, $days, $sid, $uid))
			{
				$user = JFactory::getUser($uid);
				$name = $user->name;
				$config =  JFactory::getConfig();
							
				$message = GetSubscriptionTemplate($sid);
				$message = str_replace('%username%', $name, $message);
				$message = str_replace('%sitename%', $config->getValue('sitename'), $message);
				$message = str_replace('%subname%', GetSubscriptionName($sid), $message);
				$message = str_replace('%days%', $days, $message);
				
				EmailToUser($message, $uid);
				
				$query = "insert into `#__billing_mailhistory` (subscription_id, endsubscription, days_before, send_mail, uid)".
				" values ($sid, '$newdate', $days, '$nowdate', $uid)";
				$result = $db->setQuery($query);   
				$result = $db->query();
			}
			
		}	
	}
}
	
function RenewSub($sid, $uid)	
{
	$db = JFactory::getDBO();
	$query = "select * from `#__billing_subscription_type` where id = $sid"; 
	$result = $db->setQuery($query);   
	$row = $db->loadAssoc();
	$days = $row['subscription_days'];
	if ($days == '')
	{
		$query = "select * from `#__billing_subscription_period` where subscription_id = $sid order by days limit 0, 1"; 
		$result = $db->setQuery($query);   
		$period = $db->loadAssoc();
		$days = $period['days'];
	}
	if ($days != '')
	{
		$UA = new UserAccount;	
		$ret = $UA->AddSubscription($uid, $days, $sid);
		if ($ret)
		{
			$config =  JFactory::getConfig();
			$sitename = $config->getValue('sitename');
			$message = JText::sprintf('COM_BILLING_SUB_PROLONG',$row['subscription_type'], $sitename, $days);
			EmailToUser($message, $uid);
		}
	}
}
	
	$db = JFactory::getDBO();	
	$query = "select * from `#__billing_subscription_type` order by id"; 
	$result = $db->setQuery($query);   
    $rows = $db->loadObjectList();
	if(count($rows) > 0)
	{
		foreach ($rows as $row)
		{
			if ($row->alarm_1 > 0)
			{
				NotifyCronX($row->alarm_1, $row->alarm_text, $row->id);
			}
			if ($row->alarm_2 > 0)
			{
				NotifyCronX($row->alarm_2, $row->alarm_text, $row->id);
			}
			if ($row->alarm_3 > 0)
			{
				NotifyCronX($row->alarm_3, $row->alarm_text, $row->id);
			}
		}
	}
	$dt = date('Y-m-d');
	$query = "select * from `#__billing_subscription` where subscription_id in (select id from `#__billing_subscription_type` where data2 = 'true' and is_active = 1) and enddate = '$dt' and frozen = 0"; 
	$result = $db->setQuery($query);   
    $rows = $db->loadObjectList();
	if(count($rows) > 0)
	{
		foreach ($rows as $row)
		{
			RenewSub($row->subscription_id, $row->uid);
		}
	}

?>