<?php
/*
* Joomla Billing 1.7 functions
*/
defined('_JEXEC') or die('Restricted access');

function GetSectionName($id)
{
      $db =& JFactory::getDBO();
      $query = 'select title from `#__categories` where id = ' . $id;
      $result = $db->setQuery($query);   
	  $result = $db->loadResult();
	  return $result; 
}

function GetSectionList()
{
	return false;
}

function GetComponentList()
{
      $db =& JFactory::getDBO();
      $query = "select * from `#__extensions` where enabled = 1 and type = 'component' and extension_id >= 10000 order by name";
      $result = $db->setQuery($query);   
      $rows = $db->loadObjectList();
      $out = '';
      $out .= '<select name="component_id">';
      foreach ( $rows as $row )
      {
			$out .= "<option value='$row->extension_id'>$row->name</option>"; 
      }
      $out .= '</select>';
      return $out;
}

?>