<?php
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');
require_once(JPATH_ROOT . '/components/com_billing/useraccount.php');

/**
 * Billing Component Controller

 */
 if(JoomlaVersion() > '2.5')
{
	class BillingController extends JControllerLegacy
	{
		/**
		 * Method to display the view
		 *
		 * @access    public
		 */
		function display($cachable = false, $urlparams = array())
		{
			$view = JRequest::getCmd('view');
			
			if($view == 'base')
			{
				$this->execute( JRequest::getVar( 'task' ) );
			}
			else
			{
				parent::display(false);
			}
		}

	}
	return;
}
else
{
	class BillingController extends JController
	{
		/**
		 * Method to display the view
		 *
		 * @access    public
		 */
		function display($cachable = false, $urlparams = array())
		{
			$view = JRequest::getCmd('view');
			
			if($view == 'base')
			{
				$this->execute( JRequest::getVar( 'task' ) );
			}
			else
			{
				parent::display();
			}
		}

	}
}