<?php

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
//define('DS', DIRECTORY_SEPARATOR);

jimport( 'joomla.application.component.view');
require_once( JPATH_COMPONENT.DS.'billing15.php' );

/**
 * HTML View class for the Billing Component
 */
if(JoomlaVersion() > '2.5')
{
	class BillingViewBilling extends JViewLegacy
	{
		function display($tpl = null)
		{
			$task = JRequest::getCmd('task');
			if ($task == '')
			{
				$tabno = JRequest::getInt('tabno');
				$msg = JRequest::getVar('msg');
				Cabinet($tabno, $msg);
			}
		}
	}
}
else
{
	class BillingViewBilling extends JView
	{
		function display($tpl = null)
		{
			$task = JRequest::getCmd('task');
			if ($task == '')
			{
				$tabno = JRequest::getInt('tabno');
				$msg = JRequest::getVar('msg');
				Cabinet($tabno, $msg);
			}
		}
	}
}
