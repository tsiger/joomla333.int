<?php defined('_JEXEC') or die( 'Restriction access' );

jimport( 'joomla.application.component.view');

if(JoomlaVersion() > '2.5')
{
	class BillingViewBilling extends JViewLegacy
	{
		function display($tpl = null)
		{
			parent::display($tpl);
		}
	}
}
else
{
	class BillingViewBilling extends JView
	{
		function display($tpl = null)
		{
			parent::display($tpl);
		}
	}
}
?>
