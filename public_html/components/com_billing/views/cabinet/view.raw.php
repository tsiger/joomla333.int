<?php defined('_JEXEC') or die( 'Restriction access' );

jimport( 'joomla.application.component.view');

if(JoomlaVersion() > '2.5')
{
	class BillingViewCabinet extends JViewLegacy
	{
		function display($tpl = null)
		{
			parent::display($tpl);
		}
	}
}
else
{
	class BillingViewCabinet extends JView
	{
		function display($tpl = null)
		{
			parent::display($tpl);
		}

	}
}
?>
