<?php
/**
 * Functional tests. KEEP THIS EMPTY FOR PRODUCTION RELEASES!
 *
 * @package    LookBet
 * @subpackage Controllers
 * @version    2.00.0004 $Id: test.php 91 2014-10-03 21:26:12Z dw.ilya $
 * @copyright  2014
 * @author     Created on 04-Jun-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


jimport('joomla.application.component.controller');

/**
 * Contracts controller
 *
 * @package    LookBet
 * @subpackage Controllers
 */
class LookBetControllerTest extends JControllerLegacy {
    
    public function __construct($config = array()) {

        parent::__construct($config);
    }//function
    
    public function display() {
        
        die('Not allowed.'); 
    }//function
}//class
