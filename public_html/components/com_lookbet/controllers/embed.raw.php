<?php
/**
 * @package    LookBet
 * @subpackage Controllers
 * @version    1.00.0001 $Id: embed.raw.php 58 2014-06-03 19:32:06Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 02-Jun-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


jimport('joomla.application.component.controller');

/**
 * Contracts controller
 *
 * @package    LookBet
 * @subpackage Controllers
 */
class LookBetControllerEmbed extends JControllerLegacy
{
    public function __construct($config = array()) {

        parent::__construct($config);
    }//function

    /**
     * Display stats widget contents.
     *
     * @return void
     */
    public function stats () {
        
        $jinput = JFactory::getApplication()->input;

        $jinput->set('view', 'embed');
        $jinput->set('layout', 'stats');

        parent::display();
    } // End function stats()
}//class
