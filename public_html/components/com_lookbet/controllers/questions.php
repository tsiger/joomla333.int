<?php
/**
 * @package    LookBet
 * @subpackage Controllers
 * @version    1.00.0003 $Id: questions.php 76 2014-06-25 20:42:09Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 08-May-2014
 * @since      1.00
 * @license    GNU/GPL
 *
 * @todo       Localisation
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


jimport('joomla.application.component.controller');

/**
 * Contracts controller
 *
 * @package    LookBet
 * @subpackage Controllers
 */
class LookBetControllerQuestions extends JControllerLegacy
{
    public function __construct($config = array()) {

        parent::__construct($config);
    }//function

    /**
     * Save contract data.
     *
     * @return void
     */
    public function save () {

        $this->_pushQuestion();
  
        $session = JFactory::getSession();
  
        $questions = $session->get('questions', null, 'add_questions');
  
        $model = $this->getModel('question');
  
        foreach($questions->items as $item){
            $res = $model->store($item);
        }
        
        $model->setMainQuestion($questions->id_contract);
  
        $msg = JText::_('MSG_CONTRACT_ADDED');
        $msgType = 'message';
  
        $url = JRoute::_('index.php?option=com_lookbet&task=contracts.display'); // @todo Load redirect URL from URI helper
  
        $this->setRedirect($url, $msg, $msgType);
        
    } // End function save()

    /**
     * Add contract.
     *
     * @return void
     */
    public function add () {

        $this->_pushQuestion();
  
        $view = $this->getView('question', 'html');
        $view->setLayout('add');
        $view->display();
        
    }

    private function _pushQuestion(){

        $application = JFactory::getApplication();
        $jinput = $application->input;
        $session = JFactory::getSession();
  
        $questions = $session->get('questions', false, 'add_questions');
  
        if(empty($questions) ) throw new Exception('Session registry is not available.');
  
        if(JSession::checkToken() ){
  
            $errors = false;
    
            $question = new stdClass();
    
            $question->id_contract = $questions->id_contract;
    
            $question->event_type = $jinput->post->get('event_type', null, 'raw');
            if(empty($question->event_type) ){
                $errors = true;
                $msg = JText::_('MSG_NO_EVENT_TYPE_SELECTED');
                $application->enqueueMessage($msg, 'error');
            }
    
            $question->location = $jinput->post->get('location', null, 'raw');
            if(empty($question->location) ){
                $errors = true;
                $msg = JText::_('MSG_NO_LOCATION_SELECTED');
                $application->enqueueMessage($msg, 'error');
            }
    
            $question->event_date = $jinput->post->get('event_date', null, 'raw');
            $question->event_title = $jinput->post->get('event_title', null, 'raw');
            $question->part1 = $jinput->post->get('part1', null, 'raw');
            $question->part2 = $jinput->post->get('part2', null, 'raw');
    
            $question->has_draw = $jinput->post->get('has_draw', null, 'int');
            if($question->has_draw === null){
                $errors = true;
                $msg = JText::_('MSG_NO_QUESTION_SELECTED');
                $application->enqueueMessage($msg, 'error');
            }
    
            $question->event_result = '';
    
            $question->has_image = 0;
            if($jinput->post->get('has_image', 0, 'int') == 1) $question->has_image = 1;
    
            if(!$errors){
                $questions->items[] = $question;
                $questions->next++;
            }
    
            $session->set('questions', $questions, 'add_questions');
    
        }

    } // End function add()
}//class
