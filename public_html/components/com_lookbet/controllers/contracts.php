<?php
/**
 * @package    LookBet
 * @subpackage Controllers
 * @version    3.00.0021 $Id: contracts.php 107 2014-11-21 20:16:00Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 19-Apr-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


JLoader::import('classes.common.application.controller',
                JPATH_COMPONENT_ADMINISTRATOR);

/**
 * Contracts controller
 *
 * @package    LookBet
 * @subpackage Controllers
 */
class LookBetControllerContracts extends LookBetBaseController
{
    public function __construct($config = array()) {

        parent::__construct($config);
    }//function

    /**
     * Save event results.
     *
     * @return void
     */
    public function saveres () {

        if(!JSession::checkToken() ) throw new Exception("Invalid token.");
        
        JLoader::import('helpers.uri', JPATH_COMPONENT_ADMINISTRATOR);
  
        $application = JFactory::getApplication();
        $jinput = $application->input;
  
        $idContract = $jinput->post->get('id_contract', null, 'int');
  
        if(empty($idContract) ) throw new Exception("Invalid contract ID.");
  
        $qIds = $jinput->post->get('id_question', null, 'raw');
  
        if(empty($qIds) or !is_array($qIds) ) throw new Exception("Invalid question IDs.");
  
        $model = $this->getModel('contract');
  
        foreach($qIds as $qId){
    
            $question = new stdClass();
            $question->id = 0 + $qId;
            $question->event_result = $jinput->post->get('event_result_' . $question->id, null, 'raw');
    
            if(!empty($question->event_result) ){
                $model->saveRes($question);
      
                $msg = JText::_('RESULT_SAVED');
                $application->enqueueMessage($msg, 'message');
            }
        }
        
        // Distribute bank
        $numQuestions = sizeof($model->getQuestions());
        $amount = $model->getChipsAmount();
        if ($model->hasAllResults() and !$model->getContract()->is_paid) {
            $conditions = array();
            $conditions['num_questions'] = $numQuestions;
            $conditions['id_contract'] = $idContract;

            JLoader::import('classes.bank', JPATH_COMPONENT_ADMINISTRATOR);
            $bank = LookBetBank::getInstance($amount, $conditions);
            $bank->distribute();
            $model->markAsPaid();
        }
            
        // Set redirect
        $url = LookBetUriHelper::main();
        $this->setRedirect($url);

    } // End function saveres()

    /**
     * Display results form.
     *
     * @return void
     */
    public function results () {

        $jinput = JFactory::getApplication()->input;

        $jinput->set('view', 'contract');
        $jinput->set('layout', 'results');

        parent::display();
    } // End function results()

    /**
     * Save contract data.
     *
     * @return void
     */
    public function save () {

        JLoader::import('helpers.uri', JPATH_COMPONENT_ADMINISTRATOR);
        JLoader::import('classes.params', JPATH_COMPONENT_ADMINISTRATOR);

        $jinput = JFactory::getApplication()->input;
        $session = JFactory::getSession();
        $params = LookBetParams::getInstance();
  
        if(!JSession::checkToken() ) throw new Exception("Invalid token.");

        $numQuestions = $jinput->post->get('num_questions', 0, 'int');

        if (($numQuestions < $params->get('min_questions'))
            or ($numQuestions > $params->get('max_questions')) ){
            throw new Exception("Invalid number of questions.");
        }

        $description = $jinput->post->get('description', "", 'raw');

        $user = JFactory::getUser();

        $contract = new stdClass();

        $contract->id_owner = $user->id;
        $contract->description = $description;

        $model = $this->getModel('contract');

        $questions = new stdClass();
        $questions->total = $numQuestions;
        $questions->next = 1;
        $questions->items = array();
        $questions->id_contract = $model->store($contract);
        $session->set('questions', $questions, 'add_questions');
        
        $url = LookBetUriHelper::addQuestion();
        $this->setRedirect($url);

    } // End function save()

    /**
     * Add contract.
     *
     * @return void
     */
    public function add () {

        if (!$this->isRegisteredUser()) {
            $this->loginRedirect(JText::_('MSG_LOG_IN_TO_ADD_CONTRACT'));
        } else {
            $view = $this->getView('contract', 'html');
            $view->setLayout('add');
            $view->display();
        }

    } // End function add()

    public function savevote() {
        
        JLoader::import('classes.common.image', JPATH_COMPONENT_ADMINISTRATOR);
        JLoader::import('helpers.uri', JPATH_COMPONENT_ADMINISTRATOR);
        JLoader::import('helpers.billing', JPATH_COMPONENT_ADMINISTRATOR);
        JLoader::import('helper', JPATH_COMPONENT_ADMINISTRATOR);

        $icon1x = 0;
        $icon2x = 5;
        $col1x = 35;
        $col2x = 57;
        $col3x = 127;
        $row1y = 165;
        $row2y = 195;
        $row3y = 225;
        $row4y = 255;
        $row5y = 285;
        $row6y = 315;
        $row7y = 345;
        $row8y = 375;
        $row9y = 405;
        $row10y = 435;
        $textPositions = array(
                               'num' => array('x' => 61, 'y' => 111),
                               'eventtype' => array('x' => 51, 'y' => 61),
                               'location' => array('x' => 82, 'y' => 61),
                               'eventtitle' => array('x' => 31, 'y' => 96),
                               'owner' => array('x' => 380, 'y' => 575),
                               'timestamp' => array('x' => 380, 'y' => 587),
                               'q1num' => array('x' => $col1x, 'y' => $row1y),
                               'q1parts' => array('x' => $col2x, 'y' => $row1y),
                               'q1forecast' => array('x' => $col3x, 'y' => $row1y),
                               'q1icon1' => array('x' => $icon1x, 'y' => $row1y - 16),
                               'q1icon2' => array('x' => $icon2x, 'y' => $row1y - 16),
                               'q2num' => array('x' => $col1x, 'y' => $row2y),
                               'q2parts' => array('x' => $col2x, 'y' => $row2y),
                               'q2forecast' => array('x' => $col3x, 'y' => $row2y),
                               'q2icon1' => array('x' => $icon1x, 'y' => $row2y - 16),
                               'q2icon2' => array('x' => $icon2x, 'y' => $row2y - 16),
                               'q3num' => array('x' => $col1x, 'y' => $row3y),
                               'q3parts' => array('x' => $col2x, 'y' => $row3y),
                               'q3forecast' => array('x' => $col3x, 'y' => $row3y),
                               'q3icon1' => array('x' => $icon1x, 'y' => $row3y - 16),
                               'q3icon2' => array('x' => $icon2x, 'y' => $row3y - 16),
                               'q4num' => array('x' => $col1x, 'y' => $row4y),
                               'q4parts' => array('x' => $col2x, 'y' => $row4y),
                               'q4forecast' => array('x' => $col3x, 'y' => $row4y),
                               'q4icon1' => array('x' => $icon1x, 'y' => $row4y - 16),
                               'q4icon2' => array('x' => $icon2x, 'y' => $row4y - 16),
                               'q5num' => array('x' => $col1x, 'y' => $row5y),
                               'q5parts' => array('x' => $col2x, 'y' => $row5y),
                               'q5forecast' => array('x' => $col3x, 'y' => $row5y),
                               'q5icon1' => array('x' => $icon1x, 'y' => $row5y - 16),
                               'q5icon2' => array('x' => $icon2x, 'y' => $row5y - 16),
                               'q6num' => array('x' => $col1x, 'y' => $row6y),
                               'q6parts' => array('x' => $col2x, 'y' => $row6y),
                               'q6forecast' => array('x' => $col3x, 'y' => $row6y),
                               'q6icon1' => array('x' => $icon1x, 'y' => $row6y - 16),
                               'q6icon2' => array('x' => $icon2x, 'y' => $row6y - 16),
                               'q7num' => array('x' => $col1x, 'y' => $row7y),
                               'q7parts' => array('x' => $col2x, 'y' => $row7y),
                               'q7forecast' => array('x' => $col3x, 'y' => $row7y),
                               'q7icon1' => array('x' => $icon1x, 'y' => $row7y - 16),
                               'q7icon2' => array('x' => $icon2x, 'y' => $row7y - 16),
                               'q8num' => array('x' => $col1x, 'y' => $row8y),
                               'q8parts' => array('x' => $col2x, 'y' => $row8y),
                               'q8forecast' => array('x' => $col3x, 'y' => $row8y),
                               'q8icon1' => array('x' => $icon1x, 'y' => $row8y - 16),
                               'q8icon2' => array('x' => $icon2x, 'y' => $row8y - 16),
                               'q9num' => array('x' => $col1x, 'y' => $row9y),
                               'q9parts' => array('x' => $col2x, 'y' => $row9y),
                               'q9forecast' => array('x' => $col3x, 'y' => $row9y),
                               'q9icon1' => array('x' => $icon1x, 'y' => $row9y - 16),
                               'q9icon2' => array('x' => $icon2x, 'y' => $row9y - 16),
                               'q10num' => array('x' => $col1x, 'y' => $row10y),
                               'q10parts' => array('x' => $col2x, 'y' => $row10y),
                               'q10forecast' => array('x' => $col3x, 'y' => $row10y),
                               'q10icon1' => array('x' => $icon1x, 'y' => $row10y - 16),
                               'q10icon2' => array('x' => $icon2x, 'y' => $row10y - 16)
                               );

        $image = new DWImage();
        $image->setTextPositions($textPositions);
        
        $user = JFactory::getUser();

        if(!JSession::checkToken()) throw new Exception("Invalid token.");

        $application = JFactory::getApplication();
        $jinput = $application->input;

        $questionIds = $jinput->post->get('id_question', null, 'raw');
        $bet = $jinput->post->get('bet', 0);
        $errors = false;

        if(empty($questionIds) or !is_array($questionIds)){
            $application->enqueueMessage("Invalid question IDs.", "error");
            $errors = true;
        }

        $votes = array();
        $textContents = array();
        $imageContents = array();
        $index = 1;

        $model = $this->getModel('contracts');

        foreach($questionIds as $quid) {
            if(!is_numeric($quid) or $quid <= 0){
                $application->enqueueMessage("Invalid question ID.", "error");
                $errors = true;
            }

            $quid = (int)$quid;
            $forecast = $jinput->post->get('forecast_' . $quid, null, 'word');

            if(empty($forecast)){
                $application->enqueueMessage("Invalid forecast.", "error");
                $errors = true;
            }
            
            $vote = new stdClass();
            $vote->id_question = $quid;
            $vote->forecast = $forecast;
            $vote->image_data = $jinput->files->get('image_' . $quid);
            $votes[] = $vote;
            
            // Prepare text contents to output in watermark
            $question = $model->getTable('Questions');
            $question->load($vote->id_question);
            $prefix = "q$index";
            $textContents["{$prefix}num"] = "$index.";
            $textContents["{$prefix}parts"] = $question->part1 . '-'
                . $question->part2;
            $textContents["{$prefix}forecast"] =
                LookBetHelper::getForecastType($vote->forecast,
                                               $vote->id_question);
            $locationPath = JPATH_SITE
                . "/images/lookbet/watermarks/countries/{$question->location}.png";
            $imageContents["{$prefix}icon2"] = $locationPath;

            $eventTypePath = JPATH_SITE
                . "/images/lookbet/watermarks/event_types/{$question->event_type}.png";
            $imageContents["{$prefix}icon1"] = $eventTypePath;
            
            $index++;
        }
        
        // Get contract ID from the last question processed
        $idContract = $question->id_contract;

        foreach($votes as $vote){

            $imageName = $model->saveVote($vote);

            // Image processing
            $image_data = $vote->image_data;

            if((is_array($image_data)) && ($image_data['error'] == 0)
               && ($image_data['size'] > 0)) {
                $image->getFromUpload($image_data);
                
                // Save original
                $originalImagePath = JPATH_SITE
                    . '/images/lookbet/gallery/original/' . $imageName . '.jpg';
                $image->save($originalImagePath);

                // Resize
                $image->resize(900, 600);

                // Watermark background
                $watermarkPath = JPATH_SITE
                    . '/images/lookbet/watermarks/default.png';
                $image->addWatermark($watermarkPath);
                
                // Watermark forecast gesture
                $forecastType =
                    LookBetHelper::getForecastType($vote->forecast,
                                                   $vote->id_question);
                $gesturePath = JPATH_SITE
                    . "/images/lookbet/watermarks/results/$forecastType.png";
                $image->addWatermark($gesturePath);

                // Watermark text
                $image->putTextToPosition(
                    '#'
                    . LookBetHelper::getQuestionContractId($vote->id_question),
                    'num'
                );
                
                $image->putTextToPosition($question->event_title, 'eventtitle');
                
                $image->putTextToPosition($user->name, 'owner');
                
                $timestamp = JFactory::getDate('now',
                                               $application->getCfg('offset'));
                $image->putTextToPosition($timestamp->toSql() . ' GMT', 'timestamp');

                //$locationPath = JPATH_SITE
                //    . "/images/lookbet/watermarks/countries/{$question->location}.png";
                //$image->putImageToPosition($locationPath, 'location');
                //
                //$eventTypePath = JPATH_SITE
                //    . "/images/lookbet/watermarks/event_types/{$question->event_type}.png";
                //$image->putImageToPosition($eventTypePath, 'eventtype');

                $image->putTextToPositions($textContents);
                $image->putImagesToPositions($imageContents);
                
                // Store processed image
                $galleryImagePath = JPATH_SITE . '/images/lookbet/gallery/'
                    . $imageName . '.jpg';
                $image->save($galleryImagePath);

            } else {
                // We have no image
                // Do something with it if necessary
            }
        }
        // Get contract ID from last vote
        $idContract =
            LookBetHelper::getQuestionContractId($vote->id_question);
                
        // Store bet
        if (LookBetBillingHelper::getUserBalance() >= $bet) {
            $userAccount = LookBetBillingHelper::getUserAccount();
            $userContract = $model->getTable('Usercontracts');
            $userContract->load(array('id_contract' => $idContract,
                                      'id_user' => $user->id));
            if (is_null($userContract->get('id'))) {
                $userContract->set('id_contract', $idContract);
                $userContract->set('id_user', $user->id);
            }
            if (!$userAccount->WithdrawMoney($user->id, $bet, date('His')
                                        . $user->id, 'Bet for contract #' . $idContract)) {
                $application->enqueueMessage("Cannot withdraw money", "error");
                $errors = true;
            }
            $userContract->set('bet', $bet);
            if (!$userContract->store(true)) {
                $application->enqueueMessage("Cannot store bet.", "error");
                $errors = true;
            }
        } elseif ($bet != 0) {
            $application->enqueueMessage("Cannot bet. Insufficient amount on user account.", "error");
            $errors = true;
        }

        // Report result
        if($errors){
            $url = LookBetUriHelper::addVote();
            $msg = JText::_('CANNOT_SAVE_FORECAST');
            $msg_type = "error";
        }
        else {
                $url = LookBetUriHelper::main();
                $msg = JText::_('FORECAST_SAVED');
                $msg_type = "message";
        }
        
        $this->setRedirect($url, $msg, $msg_type);
    }//function

    public function votes() {
        
        if (!$this->isRegisteredUser()) {
            $this->loginRedirect(JText::_('MSG_LOG_IN_TO_VOTE'));
        } else {
            JLoader::import('helper', JPATH_COMPONENT_ADMINISTRATOR);
            $application = JFactory::getApplication();
            $jinput = $application->input;
    
            $user = JFactory::getUser();
            $idContract = $jinput->getInt('id_contract');
    
            $jinput->set('view', 'contract');
    
            if (!LookBetHelper::hasVoted($user->id, $idContract)
                and LookBetHelper::acceptVotes($idContract)) {
                $jinput->set('layout', 'addvote');
            }
    
            parent::display();
        }
    }//function

    public function display() {

        JRequest::setVar('view', 'contracts');

        parent::display();
    }//function
}//class
