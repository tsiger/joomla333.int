<?php
/**
 * Controller for the Visit Link challenge type
 *
 * @package    LookBet
 * @subpackage Controllers
 * @version    2.00.0003 $Id: visit.php 94 2014-10-08 17:44:46Z dw.ilya $
 * @copyright  2014
 * @author     Created on 04-Oct-2014
 * @since      2.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


jimport('joomla.application.component.controller');

/**
 * Visit controller
 *
 * @package    LookBet
 * @subpackage Controllers
 */
class LookBetControllerVisit extends JControllerLegacy {
    
    public function __construct($config = array()) {

        parent::__construct($config);
    }//function
    
    public function go () {
        
        $model = $this->getModel('Visit');
        
        $row = $model->getChallengeObject(); // Load data
        $link = $row->get('link'); // Get URL
        $model->hit(); // Update hits counter
        
        $this->setRedirect($link); // Go to URL
    }
    
    public function display() {
        
        die('Not allowed.'); 
    }//function
}//class
