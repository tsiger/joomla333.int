<?php
/**
 * @package    LookBet
 * @subpackage Base
 * @version    2.00.0003 $Id: lookbet.php 95 2014-10-09 13:18:11Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 19-Apr-2014
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

//-- Initialisation
//--- Path constants
if (!defined('LOOKBET_PATH_COMPONENT_ADMINISTRATOR')) {
    define('LOOKBET_PATH_COMPONENT_ADMINISTRATOR',  JPATH_ADMINISTRATOR
           . '/components/com_lookbet');
}
if (!defined('LOOKBET_PATH_COMPONENT_SITE')) {
    define('LOOKBET_PATH_COMPONENT_SITE',  JPATH_SITE
           . '/components/com_lookbet');
}

//--- LookBet HTML extensions path
JHtml::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/classes/html');


//-- Load objects
$input = JFactory::getApplication()->input;

//-- Suppress default view
if ($input->get('view') == 'lookbet') {
    $input->set('view', 'contracts');
}

//-- Get an instance of the controller with the prefix 'LookBet'
$controller = JControllerLegacy::getInstance('LookBet');

//-- Execute the 'task' from the Request
$controller->execute($input->get('task'));

//-- Redirect if set by the controller
$controller->redirect();
