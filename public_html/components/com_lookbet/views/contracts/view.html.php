<?php
/**
 * @package    LookBet
 * @subpackage Views
 * @version    1.00.0003 $Id: view.html.php 42 2014-05-15 20:56:21Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 22-Apr-2014
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


jimport('joomla.application.component.view');

/**
 * Contracts view.
 *
 * @package    LookBet
 * @subpackage Views
 */
class LookBetViewContracts extends JViewLegacy
{
    /**
     * LookBet view display method.
     *
     * @param string $tpl The name of the template file to parse;
     *
     * @return void
     */
    public function display($tpl = null) {
        
        JHtml::stylesheet(JURI::root() . 'components/com_lookbet/assets/css/lookbet.css');
        
        $items = $this->get('Items');
        $model = $this->getModel();
        $user = JFactory::getUser();

        $this->assignRef('items', $items);
        $this->assignRef('model', $model);
        $this->assignRef('user', $user);
        
        parent::display($tpl);
    }//function
}//class
