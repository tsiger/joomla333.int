<?php
/**
 * @package    LookBet
 * @subpackage Views
 * @version    3.01.0010 $Id: default.php 108 2014-11-28 00:59:02Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 22-Apr-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

JLoader::import('helpers.uri', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::import('helper', JPATH_COMPONENT_ADMINISTRATOR);

foreach($this->items as &$item){

    $item->types = $this->model->getTypes($item->id);
    if(empty($item->types)) {
        $item->types = array();
    }
    $item->type_icons = array();

    foreach($item->types as $type){
        $item->type_icons[] = JHtml::_('image', 'images/lookbet/event_types/' . $type->event_type . '.png', $type->event_type);
    }
  
    $item->events = $this->model->getEvents($item->id);
    if(empty($item->events)) $item->events = array();
  
    $item->event_dates = $this->model->getDates($item->id);
  
    if(empty($item->event_dates)) $item->event_dates = array();
    else {
        foreach($item->event_dates as &$date){
            $date = DateTime::createFromFormat('Y-m-d', $date->event_date);
        }
    }
}

$contracts = $this->items;

?>

<?php 
$showResults = false;
foreach($contracts as $contract) {
    if ($contract->id_owner == $this->user->id) {
        $showResults = true;
    }    
}
?>

<table class="contracts">

  <thead>
    <tr>
      <th><?php echo JText::_('CONTRACT'); ?></th>
      <th><?php echo JText::_('STATUS'); ?></th>
      <th><?php echo JText::_('EVENT_TYPE'); ?></th>
      <th><?php echo JText::_('EVENTS'); ?></th>
      <th><?php echo JText::_('EVENT_DATES'); ?></th>
      <th><?php echo JText::_('NUM_QUESTIONS'); ?></th>
      <th><?php echo JText::_('PARTICIPANTS'); ?></th>
      <th><?php echo JText::_('DESCRIPTION'); ?></th>
      <?php if ($showResults) : ?><th><?php echo JText::_('CONTRACT_ADMINISTRATOR'); ?></th><?php  endif; ?>
      <th><?php echo JText::_('MY'); ?></th>
    </tr>
  </thead>

  <tbody>
    <?php foreach($contracts as $contract):
        $url = LookBetUriHelper::votes($contract->id);
        $adminUrl = LookBetUriHelper::results($contract->id);
        if ($contract->id_owner == $this->user->id) {
            $isOwner = true;
        } else {
            $isOwner = false;
        }
    ?>
      <tr>
        <td><?php echo JHtml::_('link', $url, $contract->id) ?></td>
        <td class="lb_back_status_<?php echo($contract->status); ?>"><?php echo(JText::_('STATUS_' . strtoupper($contract->status))); ?></td>
        <td>
          <?php foreach($contract->type_icons as $icon): ?>
            <?php echo $icon ?><br />
          <?php endforeach ?>
        </td>
        <td>
          <?php foreach($contract->events as $event): ?>
            <?php echo $event->event_title ?><br />
          <?php endforeach ?>
        </td>
        <td>
          <?php
            if ($contract->first_event_date and $contract->last_event_date) {
                if ($contract->first_event_date != $contract->last_event_date) {
                    // Date range
                    echo DateTime::createFromFormat('Y-m-d', $contract->first_event_date)->format(JText::_('DATE_FORMAT_LC4')); ?>-<br /><?php
                    echo DateTime::createFromFormat('Y-m-d', $contract->last_event_date)->format(JText::_('DATE_FORMAT_LC4'));
                } else {
                    // Single date
                    echo DateTime::createFromFormat('Y-m-d', $contract->first_event_date)->format(JText::_('DATE_FORMAT_LC4'));
                }
            } else {
                ?>&nbsp<?php
            }
            ?>
        </td>
        <td><?php echo $contract->num_questions ?></td>
        <td><?php echo $contract->num_votes ?></td>
        <td class="force_word_break"><?php echo $contract->description ?></td>
        <?php if ($showResults) : ?><td><?php if ($isOwner) { echo JHtml::_('link', $adminUrl, JText::_('CMD_RESULTS'));} ?></td><?php endif; ?>
        <td><?php if($contract->my_position) { ?># <?php echo($contract->my_position); ?><br />
            <?php echo($contract->my_score); ?>/<?php echo($contract->my_results_total); ?><?php } else { ?>&nbsp;<?php } ?></td>
      </tr>
    <?php endforeach ?>
  </tbody>

  <tfoot></tfoot>

</table>
