<?php
/**
 * @package    LookBet
 * @subpackage Views
 * @version    1.00.0001 $Id: default_leadergallery.php 85 2014-08-01 22:25:51Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 01-Aug-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

$score = $this->leader_gallery[0]->score;
$resultsCount = LookBetHelper::getContractResultsCount($this->contract->id);

?>

<h4><?php echo JText::_('LEADER_GALLERY') . " ($score/$resultsCount)"; ?></h4>

<div style="float:left;"><?php foreach ($this->leader_gallery as $curPos) {
    if ($curPos->id_user) {
    ?>
	<div style="float: left; text-align: center; padding: 6px;">
		<?php
                $row = $this->model->getUserImageData($curPos->id_user,
                                                      $this->contract->id);
                echo JHtml::_('lookbethtml.gallery.leaderitem', $row); ?>
	</div>
<?php   }
    }   ?>
</div>
<div style="clear:both;">
    &nbsp;</div>
    