<?php
/**
 * @package    LookBet
 * @subpackage Views
 * @version    1.00.0002 $Id: default_stats.php 58 2014-06-03 19:32:06Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 01-Jun-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)'); 

?>

<h4><?php echo JText::_('VOTING_STATS'); ?></h4>

<table border="1" width="300">
	<?php foreach($this->stats as $stat){ ?>
			<tr>
				<td align="center"><?php echo $stat['position'] ?></td>
				<td align="center"><?php echo $stat['score'].'/'.$stat['num_questions'] ?></td>
				<td align="center"><?php echo $stat['user_count'] ?></td>
			</tr>
	<?php } ?>
</table>