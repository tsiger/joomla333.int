<?php
/**
 * Form to add a new contract.
 *
 * @package    LookBet
 * @subpackage Views
 * @version    1.00.0004 $Id: add.php 76 2014-06-25 20:42:09Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 08-May-2014
 * @since      1.00
 * @license    GNU/GPL
 *
 * @todo Replace string literals with JText::_()
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

?>

<style type="text/css">

  table.params-list {
    border-collapse: collapse;
  }

  table.params-list td {
    border-style: none;
    padding: 10px;
  }

  table.params-list td.param-value {
    padding-left: 30px;
  }

</style>

<h2><?php echo JText::_('NEW_CONTRACT'); ?></h2>

<form method="post" action="<?php echo JRoute::_('index.php?option=com_lookbet&task=contracts.save') ?>">

  <table class="params-list">

    <thead></thead>

    <tbody>

      <tr>
        <td class="param-name"><?php echo JText::_('DESCRIPTION'); ?></td>
        <td class="param-value"><input type="text" name="description" /></td>
      </tr>

      <tr>

        <td class="param-name"><?php echo JText::_('NUM_QUESTIONS'); ?></td>

        <td class="param-value"><select name="num_questions">
          <?php for($i = $this->params->get('min_questions'); $i <= $this->params->get('max_questions'); $i++): ?>
            <option value="<?php echo $i ?>"<?php echo ($i == $this->params->get('default_num_questions')) ? ' selected="selected"' : '' ?>><?php echo $i ?></option>
          <?php endfor ?>
        </select></td>

      </tr>

    </tbody>

    <tfoot></tfoot>

  </table>

  <p><input type="submit" value="<?php echo JText::_('CMD_NEXT'); ?> &gt;" /></p>

  <?php echo JHtml::_('form.token') ?>

</form>
