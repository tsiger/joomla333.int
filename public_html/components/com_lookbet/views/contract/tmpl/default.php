<?php
/**
 * Default contract view: user votes and statistics.
 * 
 * @package    LookBet
 * @subpackage Views
 * @version    1.00.0013 $Id: default.php 87 2014-08-10 19:07:42Z dw.ilya $
 * @author     Created on 19-Apr-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

$numVotes = LookBetHelper::getContractVotesCount($this->contract->id);
$rewardTotal = $numVotes * $this->contract->price;

?>

<h3><?php echo JText::sprintf('CONTRACT_S', $this->contract->id); ?></h3>

<div style="float:left; width:30%;">
<p>
    <?php echo JText::_('CONTRACT_ADMINISTRATOR'); ?>: <?php echo JHtml::_('lookbethtml.cblink.link', $this->contract->id_owner); ?><br />
    <?php echo JText::_('PARTICIPANTS'); ?>: <?php echo $numVotes; ?><br />
    <?php echo JText::_('REWARD_TOTAL'); ?>: <?php echo $rewardTotal; ?><br />
    <?php echo JText::_('DESCRIPTION'); ?>: <?php echo $this->contract->description; ?><br />
</p>
</div>
<div style="float:left;">
		<?php
            $row = $this->model->getOwnerImageData($this->contract->id);
            if (!is_null($row)) {
 ?>
	<div style="float: left; text-align: center; padding: 6px;">
		<h4><?php echo JText::_('OWNER_FORECAST'); ?></h4><?php
                echo JHtml::_('lookbethtml.gallery.item', $row); ?>
	</div><?php
            }
        ?>

		<?php
            $row = $this->model->getMyImageData($this->contract->id); 
            if (!is_null($row)) {
?>
	<div style="float: left; text-align: center; padding: 6px;">
		<h4><?php echo JText::_('MY_FORECAST'); ?></h4><?php
                echo JHtml::_('lookbethtml.gallery.item', $row); ?>
	</div> <?php
            }
        ?>
	<div style="clear:both;">
		&nbsp;</div>

</div>
<div style="clear:both;">
    &nbsp;</div>
<?php if ($this->is_outdated) {
    ?><p><?php echo JText::_('MSG_NO_FORECASTS_ACCEPTED'); ?></p><?php
} ?>

<?php
// If contract_show_questions is set to 1 (always show), show questions details
// table
if ($this->params->get('contract_show_questions') == 1) {
    ?><table border="1">
	<tr>
		<th rowspan="2"><?php echo JText::_('EVENT_TYPE'); ?></th>
		<th rowspan="2"><?php echo JText::_('EVENT_LOCATION'); ?></th>
		<th rowspan="2"><?php echo JText::_('EVENT_DATE'); ?></th>
		<th rowspan="2"><?php echo JText::_('EVENT_TITLE'); ?></th>
		<th rowspan="2"><?php echo JText::_('EVENT_FORECAST'); ?></th>
		<th colspan="3"><?php echo JText::_('EVENT_RESULTS'); ?></th>
	</tr>
	<tr>		
		<th><?php echo JText::_('EVENT_FORECAST'); ?></th>
		<th><?php echo JText::_('EVENT_RESULTS'); ?></th>
		<th><?php echo JText::_('SCORE'); ?></th>
	</tr>
<?php 
	foreach($this->items as $i => $row): 

		$radioOptions = array ();
		$radioOptions[] = JHTML::_('select.option', $row->part1, $row->part1);
		if($row->has_draw) $radioOptions[] = JHTML::_('select.option', 'x', 'x');
		$radioOptions[] = JHTML::_('select.option', $row->part2, $row->part2); 
		
		if(empty($row->event_result))
			$point = '';
		elseif($row->forecast == $row->event_result) 
			$point = '1';
		else
			$point = '0';
?>	
		<tr>
			<td><img src="images/lookbet/event_types/<?php echo $row->event_type.'.png' ?>" alt=""></td>
			<td><img src="images/lookbet/countries/<?php echo $row->location.'.png' ?>" alt=""></td>
			<td><?php echo JHtml::_('date', $row->event_date, JText::_('DATE_FORMAT_LC4')) ?></td>
			<td><?php echo $row->event_title ?></td>
			<td><?php echo JHTML::_('select.radiolist', $radioOptions, 'name_'.$row->id, 'disabled', 'value', 'text', $row->forecast) ?></td>
			<td><?php echo $row->forecast ?></td>
			<td><?php echo $row->event_result ?></td>
			<td><?php echo $point ?></td>		
		</tr>		
<?php endforeach; ?>
</table>

<?php
} ?>

<?php

if (is_array($this->top_rated) and sizeof($this->top_rated) > 0) {
    echo $this->loadTemplate('ratings');
}

if (is_array($this->stats) and sizeof($this->stats) > 0) {
    echo $this->loadTemplate('stats');
}

if (is_array($this->stats) and sizeof($this->leader_gallery) > 0) {
    echo $this->loadTemplate('leadergallery');
}



$comments = JPATH_SITE.DS.'/components/com_jcomments/jcomments.php';
if (file_exists($comments)) {
	require_once($comments);
	echo JComments::showComments($this->contract->id, 'com_lookbet', $this->contract->description);
}


?>
