<?php
/**
 * @package    LookBet
 * @subpackage Views
 * @version    1.00.0007 $Id: default_ratings.php 89 2014-08-15 18:59:33Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 13-May-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

?>

<h4><?php echo JText::_('TOP_RATED'); ?></h4>

<table border="1">
    <tr>
        <th rowspan="2"><?php echo JText::_('POSITION'); ?></th>
        <th rowspan="2"><?php echo JText::_('PLAYER'); ?></th>
        <?php foreach($this->questions as $question){
                if(!$question->has_draw)
                    $separator = '-';
                else
                    $separator = '-X-';

                $questionTitle = $question->part1
                               . $separator
                               . $question->part2
                               . '<br>'
                               . ($question->event_result ? LookBetHelper::getForecastType($question->event_result, $question->id) : '?');
        ?>
                <th colspan="2">
                    <?php echo $questionTitle ?>
                </th>
        <?php } ?>
        <th rowspan="2"><?php echo JText::_('SCORE_TOTAL'); ?></th>
    </tr>
    <tr>
        <?php foreach($this->questions as $question){
            if($question->has_draw)
                $title_12 = '1X2';
            else
                $title_12 = '1-2';
            ?>
            <th><?php echo $title_12; ?></th>
            <th><?php echo JText::_('SCORE'); ?></th>
        <?php } ?>
    </tr>

    <?php
        $hasOwner = false;
        $hasMe = false;
        
        foreach($this->top_rated as $idUser => $curRow) {
        
			$userVotes = $this->model->getUserVotes($this->contract->id, $idUser);
            
            $userStatus = '';
            $rowAttribs = '';
            $isOwner = false;
            $isMe = false;
            
            if ($curRow['id_user'] == $this->user->id) {
                $isMe = true;
                $hasMe = true;
            } else {
                $isMe = false;
            }
            
            if ($curRow['id_user'] == $this->contract->id_owner) {
                $isOwner = true;
                $hasOwner = true;
            } else {
                $isOwner = false;
            }
            
            // Build user status text
            if ($isOwner and $isMe) {
                $userStatus = ' (' . JText::_('ME_INLINE') . ', ' . JText::_('OWNER_INLINE') . ')';
                $rowAttribs = ' style="font-weight: bold;"';
            } elseif ($isOwner) {
                $userStatus = ' (' . JText::_('OWNER_INLINE') . ')';
                $rowAttribs = ' style="font-weight: bold;"';
            } elseif ($isMe) {
                $userStatus = ' (' . JText::_('ME_INLINE') . ')';
                $rowAttribs = ' style="font-weight: bold;"';
            } else {
                $userStatus = '';
                $rowAttribs = '';
            }
            
            ?>
            <tr<?php echo $rowAttribs ?>>
                <td>
                    <?php echo $curRow['position'] ?>
                </td>
                <td>
                    <?php echo $curRow['user_name'] ? JHtml::_('lookbethtml.cblink.link', $idUser) . $userStatus : '&nbsp;'; ?>
                </td>
                <?php foreach($this->questions as $question) { ?>
						<td>
						<?php foreach($userVotes as $userVote){ 
								if($userVote->id == $question->id){
									echo LookBetHelper::getForecastType($userVote->forecast, $question->id);
									break;
								}
							  }	?>
						</td>
						<td>
						<?php foreach($userVotes as $userVote) { 
								if($userVote->id == $question->id){
									echo $userVote->score;
									break;
								} 
							  } ?>
						</td>
				<?php } ?>
                <td>
                    <?php echo $curRow['score'] ?>
                </td>
            </tr>
    <?php }
    
    if ($this->my_rated and !$hasMe) {
        $user = JFactory::getUser();
        $idUser = $user->id;
        $curRow = $this->my_rated[$idUser];
        $userVotes = $this->model->getUserVotes($this->contract->id, $idUser); ?>
        <tr style="font-weight: bold;">
            <td>
                <?php echo $curRow['position'] ?>
            </td>
            <td>
                <?php echo $curRow['user_name'] ? JHtml::_('lookbethtml.cblink.link', $idUser) . ' (' . JText::_('ME_INLINE') . ')' : '&nbsp;'; ?>
            </td>
            <?php foreach($this->questions as $question) { ?>
                    <td>
                    <?php foreach($userVotes as $userVote){ 
                            if($userVote->id == $question->id) {
                                echo LookBetHelper::getForecastType($userVote->forecast, $question->id);
                                break;
                            }
                          }	?>
                    </td>
                    <td>
                    <?php foreach($userVotes as $userVote) { 
                            if($userVote->id == $question->id){
                                echo $userVote->score;
                                break;
                            } 
                          } ?>
                    </td>
            <?php } ?>
            <td>
                <?php echo $curRow['score'] ?>
            </td>
        </tr>
    <?php }    
    
    if ($this->owner_rated and !$hasOwner) {
        $user = JFactory::getUser();
        $idUser = LookBetHelper::getContractOwner($this->contract->id);
        $curRow = $this->owner_rated[$idUser];
        $userVotes = $this->model->getUserVotes($this->contract->id, $idUser); ?>
        <tr style="font-weight: bold;">
            <td>
                <?php echo $curRow['position'] ?>
            </td>
            <td>
                <?php echo $curRow['user_name'] ? JHtml::_('lookbethtml.cblink.link', $idUser) . ' (' . JText::_('OWNER_INLINE') . ')' : '&nbsp;'; ?>
            </td>
            <?php foreach($this->questions as $question) { ?>
                    <td>
                    <?php foreach($userVotes as $userVote){ 
                            if($userVote->id == $question->id) {
                                echo LookBetHelper::getForecastType($userVote->forecast, $question->id);
                                break;
                            }
                          }	?>
                    </td>
                    <td>
                    <?php foreach($userVotes as $userVote) { 
                            if($userVote->id == $question->id){
                                echo $userVote->score;
                                break;
                            } 
                          } ?>
                    </td>
            <?php } ?>
            <td>
                <?php echo $curRow['score'] ?>
            </td>
        </tr>
    <?php } ?>

</table>
