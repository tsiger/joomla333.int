<?php
/**
 * @package    LookBet
 * @subpackage Views
 * @version    3.04.0011 $Id: addvote.php 105 2014-11-11 17:30:18Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 19-Apr-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

JLoader::import('helpers.uri', JPATH_COMPONENT_ADMINISTRATOR);

JHtml::_('behavior.formvalidation');

$numVotes = LookBetHelper::getContractVotesCount($this->contract->id);
$rewardTotal = $numVotes * $this->contract->price;

?>

<script type="text/javascript">
    function changeStatus(status) {
        jQuery("#add_vote input").each(function() {
            this.disabled = !status;
        });
    }
    var checkboxClicked = function() {
        var status = this.checked;
        changeStatus(status);
    };
    jQuery( document ).ready(function() {
        if (status = jQuery('#make_bet').is(":checked"))
        {
          changeStatus(status);
        }
        jQuery( '#make_bet' ).on( 'click', function() {
            var status = this.checked;
            changeStatus(status);
        });
    });
 
</script>

<h3><?php echo JText::sprintf('CONTRACT_S', $this->contract->id); ?></h3>

<p>
    <?php echo JText::_('CONTRACT_ADMINISTRATOR'); ?>: <?php echo JHtml::_('lookbethtml.cblink.link', $this->contract->id_owner); ?><br />
    <?php echo JText::_('DATE_ADDED'); ?>: <?php echo JHtml::_('date', $this->contract->date_added, JText::_('DATE_FORMAT_LC4')); ?><br />
    <?php echo JText::_('FEE'); ?>: <?php echo $this->contract->price; ?><br />
    <?php echo JText::_('PARTICIPANTS'); ?>: <?php echo $numVotes; ?><br />
    <?php echo JText::_('REWARD_TOTAL'); ?>: <?php echo $rewardTotal; ?><br />
    <?php echo JText::_('DESCRIPTION'); ?>: <?php echo $this->contract->description; ?><br />
</p>

<form enctype="multipart/form-data" method="post" action="<?php echo LookBetUriHelper::saveVote() ?>" class="form-validate">
    <p><input id="make_bet" type="checkbox" name="make_bet" value="1" style="position: static;"></p>
    <div id="add_vote">
        <?php /*if ($this->get('UserBalance') >= $this->contract->price) {?><input type="checkbox" name="bet" id="bet" value="<?php echo($this->get('UserBalance'))?>" /><label for="bet"><?php echo(JText::sprintf('BET_S_S_YOU_HAVE_S_S', $this->get('DefaultCurrency'), $this->contract->price, $this->get('UserBalance'))); ?></label><?php }
        else {
        echo(JText::_('YOU_CANNOT_BET') . ' ' . JText::sprintf('YOU_NEED_S_S_TO_BET_YOUR_BALANCE_IS_S_S', $this->get('DefaultCurrency'), $this->contract->price, $this->get('UserBalance')));
        }*/
        echo(JHtml::_('lookbethtml.contract.bet', $this->contract->price,
                      $this->get('DefaultCurrency'), $this->get('UserBalance')));
        ?>
        
        <table border="1">
        
        <tr>
            <th><?php echo JText::_('EVENT_TYPE'); ?></th><?php $colCount = 1; ?>
            <th><?php echo JText::_('EVENT_LOCATION'); ?></th><?php $colCount++; ?>
            <th><?php echo JText::_('EVENT_DATE'); ?></th><?php $colCount++; ?>
            <th><?php echo JText::_('EVENT_TITLE'); ?></th><?php $colCount++; ?>
            <th><?php echo JText::_('EVENT_FORECAST'); ?></th><?php $colCount++; ?>
        </tr>
        
        <?php
        
          foreach($this->items as $i => $row):
        
            $radioOptions = array ();
            $radioOptions[] = JHTML::_('select.option', $row->part1, $row->part1);
            if($row->has_draw) $radioOptions[] = JHTML::_('select.option', 'x', 'x');
            $radioOptions[] = JHTML::_('select.option', $row->part2, $row->part2);
        
            if(empty($row->event_result))
                $point = '';
            elseif($row->forecast == $row->event_result)
                $point = '1';
            else
                $point = '0';
        
        ?>
        
            <tr>
            
                <td><img src="images/lookbet/event_types/<?php echo $row->event_type . '.png' ?>" alt=""></td>
                <td><img src="images/lookbet/countries/<?php echo $row->location . '.png' ?>" alt=""></td>
                <td><?php echo JHtml::_('date', $row->event_date, JText::_('DATE_FORMAT_LC4')) ?></td>
                <td><?php echo $row->event_title ?></td>
                <td>
                  <?php echo JHTML::_('select.radiolist', $radioOptions, 'forecast_'.$row->id, array('required' => 'required', 'disabled' => 'disabled'), 'value', 'text', $row->forecast) ?>
                  <input type="hidden" name="id_question[]" value="<?php echo $row->id ?>" />
                </td>
                
            </tr>
            
        <?php if($row->has_image == 1): ?>
            <tr>
                <td colspan="<?php echo($colCount); ?>"><input type="file" name="image_<?php echo $row->id ?>" required="required" disabled="disabled" /></td>
            </tr>
        <?php endif; ?>
        
        <?php endforeach; ?>
        </table>
        
        <?php echo JHtml::_('form.token') ?>
        
        <input type="submit" value="<?php echo JText::_('CMD_SAVE'); ?>"  disabled="disabled" />
    </div>
</form>

