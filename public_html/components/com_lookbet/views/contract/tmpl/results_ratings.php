<?php
/**
 * @package    LookBet
 * @subpackage Views
 * @version    1.00.0001 $Id$
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 13-May-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

$numVotes = LookBetHelper::getContractVotesCount($this->contract->id);
$rewardTotal = $numVotes * $this->contract->price;

?>

<h3><?php echo JText::sprintf('CONTRACT_S', $this->contract->id); ?></h3>

<p>
    <?php echo JText::_('CONTRACT_ADMINISTRATOR'); ?>: <?php echo $this->contract->owner; ?><br />
    <?php echo JText::_('DATE_ADDED'); ?>: <?php echo JHtml::_('date', $this->contract->date_added, JText::_('DATE_FORMAT_LC')); ?><br />
    <?php echo JText::_('FEE'); ?>: <?php echo $this->contract->price; ?><br />
    <?php echo JText::_('PARTICIPANTS'); ?>: <?php echo $numVotes; ?><br />
    <?php echo JText::_('REWARD_TOTAL'); ?>: <?php echo $rewardTotal; ?><br />
    <?php echo JText::_('DESCRIPTION'); ?>: <?php echo $this->contract->description; ?><br />
</p>

<form enctype="multipart/form-data" method="post" action="<?php echo JRoute::_('index.php?option=com_lookbet&task=contracts.saveres') ?>">

  <table border="1">

    <tr>
      <th><?php echo JText::_('EVENT_TYPE'); ?></th>
      <th><?php echo JText::_('EVENT_LOCATION'); ?></th>
      <th><?php echo JText::_('EVENT_DATE'); ?></th>
      <th><?php echo JText::_('EVENT_TITLE'); ?></th>
      <th><?php echo JText::_('EVENT_FORECAST'); ?></th>
    </tr>

    <?php

      $form_disable = true;

      foreach($this->items as $i => $row):

        $radioOptions = array ();
        $radioOptions[] = JHTML::_('select.option', $row->part1, $row->part1);
        if($row->has_draw) $radioOptions[] = JHTML::_('select.option', 'x', 'x');
        $radioOptions[] = JHTML::_('select.option', $row->part2, $row->part2);

        if(empty($row->event_result) ) $form_disable = false;

    ?>

        <tr>

          <td><img src="images/lookbet/event_types/<?php echo $row->event_type . '.png' ?>" alt=""></td>
          <td><img src="images/lookbet/countries/<?php echo $row->location . '.png' ?>" alt=""></td>
          <td><?php echo JHtml::_('date', $row->event_date, JText::_('DATE_FORMAT_LC')) ?></td>
          <td><?php echo $row->event_title ?></td>
          <td>

            <?php echo JHTML::_('select.radiolist', $radioOptions, 'event_result_'.$row->id, ( (!empty($row->event_result) ) ? array('disabled'=>'disabled') : null), 'value', 'text', ( (!empty($row->event_result) ) ? $row->event_result : null) ) ?>

            <?php if(empty($row->event_result) ): ?>
              <input type="hidden" name="id_question[]" value="<?php echo $row->id ?>" />
            <?php endif ?>

          </td>

        </tr>

    <?php endforeach; ?>

  </table>

  <?php echo JHtml::_('form.token') ?>

  <input type="hidden" name="id_contract" value="<?php echo $this->contract->id ?>" />

  <hr />

  <input type="submit"<?php echo ($form_disable) ? ' disabled="disabled"' : '' ?> value="<?php echo JText::_('CMD_SAVE'); ?>" />

</form>
