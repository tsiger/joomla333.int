<?php
/**
 * @package    LookBet
 * @subpackage Views
 * @version    3.00.0018 $Id: view.html.php 105 2014-11-11 17:30:18Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 19-Apr-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


jimport('joomla.application.component.view');

/**
 * Contract view.
 *
 * @package    LookBet
 * @subpackage Views
 */
class LookBetViewContract extends JViewLegacy
{
    protected $_validLayouts = array('addvote', 'add', 'default', 'results');
    /**
     * LookBets view display method.
     *
     * @param string $tpl The name of the template file to parse;
     *
     * @return void
     */
    public function display($tpl = null) {

        JLoader::import('helper', JPATH_COMPONENT_ADMINISTRATOR);
        JLoader::import('classes.params', JPATH_COMPONENT_ADMINISTRATOR);

        $topRated = null;
        $stats = null;
        $questions = null;
        $model = null;
        $isOutdated = false;
        
        $jinput = JFactory::getApplication()->input;
        $params = LookBetParams::getInstance();
        $user = JFactory::getUser();
        
        $layout = $jinput->getCmd('layout', null);
        $idContract = $jinput->getInt('id_contract', null);

        if (in_array($layout, $this->_validLayouts)) {
            $this->setLayout($layout);
        }

        $tmpl = $this->getLayout();

        if($tmpl == 'add') {
            // Set variables for form there if necessary
        } else {
            switch ($layout) {
                case 'results':
                case 'addvote':
                    $items = $this->get('Questions');
                    break;
      
                case 'default':
                default:
                    $items = $this->get('Votes');
                    $topRated = $this->get('TopRated');
                    $myRated = $this->get('MyRated');
                    $ownerRated = $this->get('OwnerRated');
                    $stats = $this->get('Stats');
                    $questions = $this->get('Questions');
                    $model = $this->getModel('Contract');
                    $leaderGallery = $this->get('LeaderGallery');
                    
                    if (!LookBetHelper::acceptVotes($idContract)
                        and !LookBetHelper::hasVoted($user->id, $idContract)) {
                        // Stop voting for this event!
                        $isOutdated = true;
                    }
                    break;
            }
            $contract = $this->get('Contract');
  
            $this->assignRef('items', $items);
            $this->assignRef('contract', $contract);
            $this->assignRef('top_rated', $topRated);
            $this->assignRef('my_rated', $myRated);
            $this->assignRef('owner_rated', $ownerRated);
            $this->assignRef('stats', $stats);
            $this->assignRef('questions', $questions);
            $this->assignRef('model', $model);
            $this->assign('is_outdated', $isOutdated);
            $this->assignRef('leader_gallery', $leaderGallery);
            $this->assignRef('user', $user);
        }
        $this->assignRef('params', $params);

        parent::display($tpl);
    }//function
}//class
