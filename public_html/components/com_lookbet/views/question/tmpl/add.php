<?php
/**
 * Form to add a new question.
 *
 * @package    LookBet
 * @subpackage Views
 * @version    1.00.0007 $Id: add.php 86 2014-08-07 21:04:15Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 08-May-2014
 * @since      1.00
 * @license    GNU/GPL
 * 
 * @todo       Replace raw selects with ones from JHtml
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

JHtml::_('behavior.formvalidation');

$session = JFactory::getSession();

$questions = $session->get('questions', false, 'add_questions');

if(!$questions) throw new Exception('Empty questions data');

$formAction = 'add';
$submitText = JText::_('CMD_NEXT') . ' &gt;';
if($questions->next == $questions->total){
    $formAction = 'save';
    $submitText = JText::_('CMD_SAVE');
}

// @todo We need some helper there to avoid DB quering in template

$db = JFactory::getDbo();

$query = "SELECT `event_type` FROM `#__lookbet_event_types`";

$db->setQuery($query);

$eventTypes = $db->loadColumn(0);

$query = "SELECT `location` FROM `#__lookbet_locations`";

$db->setQuery($query);

$locations = $db->loadColumn(0);

JHtml::_('behavior.calendar');
$now = new DateTime();
$now = $now->format('Y-m-d H:i:s');

?>

<style type="text/css">

  table.params-list {
    border-collapse: collapse;
  }

  table.params-list td {
    border-style: none;
    padding: 10px;
  }

  table.params-list td.param-value {
    padding-left: 30px;
  }

</style>

<h2><?php JText::printf('ADDING_QUESTION_S_OF_S', $questions->next, $questions->total); ?></h2>

<form method="post" action="<?php echo JRoute::_('index.php?option=com_lookbet&task=questions.' . $formAction) ?>" class="form-validate">

  <table class="params-list">

    <thead></thead>

    <tbody>

      <tr>
        <td class="param-name"><?php echo JText::_('EVENT_TYPE'); ?></td>
        <td class="param-value"><select name="event_type" required="required">
          <option value="" selected="selected">-<?php echo JText::_('SELECT_EVENT_TYPE'); ?>-</option>
          <?php foreach($eventTypes as $type): ?>
            <option value="<?php echo $type ?>"><?php echo $type ?></option>
          <?php endforeach ?>
        </select></td>
      </tr>

      <tr>
        <td class="param-name"><?php echo JText::_('EVENT_LOCATION_FULL'); ?></td>
        <td class="param-value"><select name="location" required="required">
          <option value="" selected="selected">-<?php echo JText::_('SELECT_LOCATION'); ?>-</option>
          <?php foreach($locations as $location): ?>
            <option value="<?php echo $location ?>"><?php echo $location ?></option>
          <?php endforeach ?>
        </select></td>
      </tr>

      <tr>
        <td class="param-name"><?php echo JText::_('EVENT_DATE_FULL'); ?></td>
        <td class="param-value"><?php echo JHtml::_('calendar', JHtml::_('date', $now, JText::_('DATE_FORMAT_LC1')), "event_date", "event_date", '%Y-%m-%d', null) ?></td>
      </tr>

      <tr>
        <td class="param-name"><?php echo JText::_('EVENT_TITLE'); ?></td>
        <td class="param-value"><input type="text" name="event_title" required="required" /></td>
      </tr>

      <tr>
        <td class="param-name"><?php echo JText::_('QUESTION'); ?></td>
        <td class="param-value"><select name="has_draw" required="required">
          <option value="" selected="selected">-<?php echo JText::_('SELECT_QUESTION'); ?>-</option>
          <option value="0"><?php echo JText::_('RESULT_1_2'); ?></option>
          <option value="1"><?php echo JText::_('RESULT_1_X_2'); ?></option>
        </select></td>
      </tr>

      <tr>
        <td class="param-name"><?php echo JText::_('HOME_PART'); ?></td>
        <td class="param-value"><input type="text" name="part1" required="required" /></td>
      </tr>
      <tr>
        <td class="param-name"><?php echo JText::_('VISITORS_PART'); ?></td>
        <td class="param-value"><input type="text" name="part2" required="required" /></td>
      </tr>

    </tbody>

    <tfoot></tfoot>

  </table>

  <p><input type="submit" value="<?php echo $submitText ?>" class="validate" /></p>

  <?php echo JHtml::_('form.token') ?>

</form>
