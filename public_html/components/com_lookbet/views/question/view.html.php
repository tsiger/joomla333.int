<?php
/**
 * @package    LookBet
 * @subpackage Views
 * @version    1.00.0001 $Id: view.html.php 31 2014-05-08 10:43:52Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 08-May-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


jimport('joomla.application.component.view');

/**
 * Contract view.
 *
 * @package    LookBet
 * @subpackage Views
 */
class LookBetViewQuestion extends JViewLegacy
{
    protected $_validLayouts = array('add');
    
    /**
     * LookBets view display method.
     *
     * @param string $tpl The name of the template file to parse;
     *
     * @return void
     */
    public function display($tpl = null) {
        
        JLoader::import('helper', JPATH_COMPONENT_ADMINISTRATOR);
        
        $layout = JRequest::getCmd('layout', null);

        if (in_array($layout, $this->_validLayouts)) {
            $this->setLayout($layout);
        }
        
        parent::display($tpl);
    }//function
}//class
