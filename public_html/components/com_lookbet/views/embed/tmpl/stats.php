<?php
/**
 * @package    LookBet
 * @subpackage Views
 * @version    1.00.0001 $Id: stats.php 58 2014-06-03 19:32:06Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 02-Jun-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');

$this->addTemplatePath(JPATH_COMPONENT_SITE . '/views/contract/tmpl');
$this->setLayout('default');
echo $this->loadTemplate('ratings');
echo $this->loadTemplate('stats');
?>