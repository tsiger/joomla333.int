<?php
/**
 * @package    LookBet
 * @subpackage Views
 * @version    1.00.0001 $Id: view.raw.php 58 2014-06-03 19:32:06Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 02-Jun-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


jimport('joomla.application.component.view');

/**
 * Contract view.
 *
 * @package    LookBet
 * @subpackage Views
 */
class LookBetViewEmbed extends JViewLegacy
{
    protected $_validLayouts = array('stats');
    /**
     * LookBet Embed view display method.
     *
     * @param string $tpl The name of the template file to parse;
     *
     * @return void
     */
    public function display($tpl = null) {

        JLoader::import('helper', JPATH_COMPONENT_ADMINISTRATOR);
        
        $topRated = null;
        $stats = null;
        $questions = null;
        $model = null;
        
        $model = JModelLegacy::getInstance('Contract', 'LookBetModel');
        $this->setModel($model, true);
        
        $layout = JRequest::getCmd('layout', null);
        
        if (in_array($layout, $this->_validLayouts)) {
            $this->setLayout($layout);
        }
        
        $tmpl = $this->getLayout();
        
        if($tmpl == 'add'){
          // Set variables for form there if necessary
        } else {
            switch ($layout) {
                case 'stats':
                default:
                    $items = $this->get('Votes');
                    $topRated = $this->get('TopRated');
                    $stats = $this->get('Stats');
                    $questions = $this->get('Questions');
                    break;
            }
            $contract = $this->get('Contract');
        
            $this->assignRef('items', $items);
            $this->assignRef('contract', $contract);
            $this->assignRef('top_rated', $topRated);
            $this->assignRef('stats', $stats);
            $this->assignRef('questions', $questions);
            $this->assignRef('model', $model);
        }

        parent::display($tpl);
    }//function
}//class
