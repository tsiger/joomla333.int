<?php
/**
 * @package    LookBet
 * @subpackage Views
 * @version    1.00.0002 $Id: view.html.php 21 2014-04-30 18:12:25Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 19-Apr-2014
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


/**
 * HTML View class for the LookBet Component.
 *
 * @package LookBet
 */
class LookBetViewLookBet extends JViewLegacy
{
/* **************************************************************************
 * NOT USED SO FAR! DO NOT PLACE METHODS HERE!
 * See elsewhere.
 * НЕ ИСПОЛЬЗУЕТСЯ! КОД СЮДА НЕ ДОБАВЛЯТЬ!
 * Актуальный код - в других файлах.
 * ************************************************************************** */

    
    ///**
    // * LookBet view display method.
    // *
    // * @param string $tpl The name of the template file to parse;
    // *
    // * @return void
    // */
    //public function display($tpl = null)
    //{
    //    $this->allData = $this->get('All');
    //
    //    parent::display($tpl);
    //}
}
