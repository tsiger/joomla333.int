<?php
/**
 * @package    LookBet
 * @subpackage Base
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @author     Created on 19-Apr-2014
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


/**
 * LookBet Controller.
 *
 * @package    LookBet
 * @subpackage Controllers
 */
class LookBetController extends JControllerLegacy
{
}
