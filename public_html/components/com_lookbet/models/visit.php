<?php
/**
 * @package    LookBet
 * @subpackage Models
 * @version    3.02.0006 $Id: visit.php 119 2014-12-09 00:48:45Z dw.ilya $
 * @copyright  2014
 * @author     Created on 04-Oct-2014
 * @since      2.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


jimport('joomla.application.component.model');

/**
 * Vote model.
 *
 * @package    LookBet
 * @subpackage Models
 * @todo        Make parent model class for all challenge types and move common methods there.
 */
class LookBetModelVisit extends JModelLegacy
{
    protected $_id;
    protected $_params;
    
    /**
     * Constructor.
     */
    public function __construct() {
        
        
        parent::__construct();
    }//function
    
    /**
     * Get LookBet parameters object
     * 
     * @return LookBetParams
     */
    protected function & _getParams () {
    
        if (!isset($this->_params)) {
            JLoader::import('classes.params',
                            LOOKBET_PATH_COMPONENT_ADMINISTRATOR);
            $this->_params = LookBetParams::getInstance();
        }
    
        return $this->_params;
    } // End function _getParams()
    
    /**
     * Get LookBet-specific group properties
     *
     * @param integer $idGroup Group ID
     * @return JTable or false on error
     */
    public function & getGroupProperties ($idGroup) {
    
        $result = null;
    
        if (!$result = $this->getTable('GroupProperties')) {
            // @todo ERROR: Cannot get group table
            return false;
        }
        
        if (!$result->load($idGroup)) {
            // @todo ERROR: Cannot load group data
            return false;
        }
    
        return $result;
    } // End function getGroupProperties()
    
    /**
     * Set challenge completion flag for current user.
     * 
     * @return boolean false on error
     */
    public function completeChallenge () {
    
        $result = null;
    
        $user = JFactory::getUser();
        
        if ($user->id) {
            $row = $this->getTable('UserChallenges');
            $row->set('id_user', $user->id);
            $row->set('id_challenge', $this->getId());
            $row->set('is_complete', 1);
            
            $result = $row->store();
        }
    
        return $result;
    } // End function completeChallenge()
    
    /**
     * Get challenge ID from HTTP query.
     * 
     * @return integer
     */
    public function getId () {
    
        if (!isset($this->_id)) {
            $application = JFactory::getApplication();
            $jinput = $application->input;
            
            $this->_id = $jinput->getInt('id', null);
        }
    
        return $this->_id;
    } // End function getId()
    
    /**
     * Checks if given challenge is completed by user.
     * 
     * @return boolean
     */
    public function isChallengeComplete () {
    
        $result = null;
    
        //$db = $this->getDbo();
        //$query = $db->getQuery(true);
        //$user = JFactory::getUser();
        //
        //$query
        //    ->select('*')
        //    ->from('#__lookbet_user_challenges')
        //    ->where('id_user = ' . $user->id)
        //    ->where('id_challenge = ' . $this->getId());
        //$db->setQuery($query);
        //$result = (boolean)(sizeof($db->loadAssocList()) > 0);
        
        JLoader::import('classes.challenges', JPATH_COMPONENT_ADMINISTRATOR);
        
        $row = $this->getChallengeObject();
        $challenges = LookBetChallenges::getInstance($row->challenge->id_group);
        $result = $challenges->isComplete($this->getId());
    
        return $result;
    } // End function isChallengeComplete()
    
    /**
     * Get the challenge data row.
     * 
     * @return JTable or false on error
     */
    public function & getChallengeObject () {
    
        $result = null;
    
        $id = $this->getId();
        
        $challenge = $this->getTable('Challenges');
        $challenge->load($id);
        $idObject = $challenge->get('id_object');
        $type = $challenge->get('challenge_type');
        $result = $this->getTable('Challenge' . ucfirst($type) . 's');
        $result->load($idObject);
        $result->challenge = $challenge;
    
        return $result;
    } // End function getChallengeObject()
    
    /**
     * Checks if given user is an owner of the group
     * 
     * @return boolean
     */
    public function isOwner ($idGroup, $idUser) {
    
        $result = false;
    
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        
        $query
            ->select('user_id')
            ->from('#__groupjive_groups')
            ->where('id = ' . $idGroup)
            ->set('limit', 1);
        $db->setQuery($query);
        $idOwner = $db->loadResult();
        
        if ($idUser == $idOwner) {
            $result = true;
        }
    
        return $result;
    } // End function isOwner()
    
    /**
     * Update link hits counter.
     * 
     * @return false on error
     */
    public function hit () {
    
        $user = JFactory::getUser();
        
        if ($user->id and !$this->isChallengeComplete()) {
            $row = $this->getChallengeObject();
            if (!$group = $this->getGroupProperties($row->challenge->id_group)) {
                // @todo ERROR: Cannot load group
                return false;
            }
            if (!$this->isOwner($group->id, $user->id)) {
                if (!$group->payToUser($this->_getParams()
                                        ->get('challenge_payment'))) {
                    // @todo ERROR: Cannot move points to user
                    return false;
                }
                if ($this->completeChallenge()) {
                    $row->hit();
                }
            }
        }
    
        return true;
    } // End function hit()
}//class
