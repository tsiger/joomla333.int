<?php
/**
 * @package    LookBet
 * @subpackage Models
 * @version    1.00.0003 $Id: gallery.php 85 2014-08-01 22:25:51Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 04-Jun-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


jimport('joomla.application.component.model');

/**
 * Vote model.
 *
 * @package    LookBet
 * @subpackage Models
 */
class LookBetModelGallery extends JModelLegacy
{
    /**
     * Constructor.
     */
    public function __construct() {
        
        parent::__construct();
    }//function
    
    /**
     * Get the array of user votes with images.
     *
     * @param integer $idUser User ID
     * @return array
     */
    public function & getUserImages ($idUser) {
    
        $result = array();

        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        $query->select($db->qn(array('v.id', 'v.id_question', 'q.id_contract')))
              ->from($db->qn('#__lookbet_votes', 'v'))
              ->join('INNER', $db->qn('#__lookbet_questions', 'q') . 'ON' . '(' . $db->qn('v.id_question') . ' = ' . $db->qn('q.id') . ')')
              ->where($db->qn('v.id_user') . ' = ' . (int)$idUser . ' AND ' . $db->qn('q.has_image') . ' = ' . 1);

        $db->setQuery($query);

        $result = $db->loadObjectList();

        return $result;
    } // End function getUserImages()
}//class
