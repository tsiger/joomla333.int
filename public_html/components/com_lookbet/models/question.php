<?php
/**
 * @package    LookBet
 * @subpackage Models
 * @version    1.00.0002 $Id: question.php 70 2014-06-18 19:35:54Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 08-May-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


jimport('joomla.application.component.model');

/**
 * Vote model.
 *
 * @package    LookBet
 * @subpackage Models
 */
class LookBetModelQuestion extends JModelLegacy
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        // Additional work
        $foo = 'Bar';

        parent::__construct();
    }//function

    /**
     * Mark the latest event as question with photo.
     *
     * @param integer $idContract Contract ID
     * @return void
     */
    public function setMainQuestion ($idContract) {
    
        //$db = new JDatabase(); // ## @todo REMOVE THIS!!!
        $db = $this->getDbo();
        
        //$query = new JDatabaseQuery(); // ## @todo REMOVE THIS!!!
        // Get last question ID
        $query = $db->getQuery(true);
        $query
            ->select($db->quoteName('id'))
            ->from($db->quoteName('#__lookbet_questions'))
            ->where($db->quoteName('id_contract') . " = $idContract")
            ->order($db->quoteName('event_date') . ' DESC')
            ->order($db->quoteName('id') . ' DESC');
        $db->setQuery($query);
        $lastId = $db->loadResult();
        
        if ($lastId) {
            $query = $db->getQuery(true);
            $query
                ->update('#__lookbet_questions')
                ->set($db->quoteName('has_image') . ' = 1')
                ->where(array($db->quoteName('id_contract') .  " = $idContract",
                              $db->quoteName('id') . " = $lastId"));
            $db->setQuery($query);
            $db->execute(); // @todo Add error processing here
        } else {
            // @todo Add error processing here
        }
    } // End function setMainQuestion()
    
    /**
     * Save question data.
     *
     * @param array $data form data from request
     * @return boolean false on error
     */
    function store ($data) {

        JModelLegacy::addTablePath(JPATH_ADMINISTRATOR . '/components/com_lookbet/tables');

        $row = $this->getTable('questions');

        if (!$row->bind($data)) {
            return false;
        }

        if (!$row->check()) {
            return false;
        }

        if (!$row->store(true)) {
            return false;
        }

        return true;
    } // function store()
}//class
