<?php
/**
 * @package    LookBet
 * @subpackage Models
 * @version    3.00.0028 $Id: contract.php 107 2014-11-21 20:16:00Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 19-Apr-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


jimport('joomla.application.component.model');
JLoader::import('helpers.billing', JPATH_COMPONENT_ADMINISTRATOR);

/**
 * Vote model.
 *
 * @package    LookBet
 * @subpackage Models
 */
class LookBetModelContract extends JModelLegacy

{
    /**
     * Constructor.
     */
    public function __construct()
    {
        // Additional work
        
        parent::__construct();
    }//function

    
    /**
     * Set payment flag for current contract.
     * 
     * @return false on error
     */
    public function markAsPaid () {
    
        $result = null;
    
        $row = $this->getTable('Contracts');
        if (!$row->load($this->getContractId())) {
            // @todo ERROR: Cannot load contract data.
            return false;
        }
        $row->set('is_paid', 1);
        $result = $row->store(true);
    
        return $result;
    } // End function markAsPaid()
    
    /**
     * Get contract ID from form input.
     * 
     * @return integer
     */
    protected function getContractId () {
    
        static $result;
    
        if (!isset($result)) {
            $jinput = JFactory::getApplication()->input;
            
            $result = $jinput->getInt('id_contract');
        }
        
        return $result;
    } // End function getContractId()
    
    /**
     * Check if this contract has all the results set.
     * 
     * @return boolean
     */
    public function hasAllResults () {
    
        $result = null;
    
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        
        $query
            ->select('COUNT(id)')
            ->from('#__lookbet_questions')
            ->where('id_contract = ' . $this->getContractId())
            ->where("(event_result = '' OR event_result IS NULL)");
        $db->setQuery($query);
        $count = $db->loadResult();
        
        if ($count == 0) {
            $result = true;
        } else {
            $result = false;
        }
    
        return $result;
    } // End function hasAllResults()
    
    /**
     * Get the amount of chips in contract bank.
     * 
     * @return numeric
     */
    public function getChipsAmount () {
    
        $result = null;
        
        $jinput = JFactory::getApplication()->input;
        
        $idContract = $jinput->getInt('id_contract');
        
        $db = $this->getDbo();
        
        $query = $db->getQuery(true);
        $query
            ->select('SUM(bet)')
            ->from('#__lookbet_user_contracts')
            ->where("id_contract = $idContract");
        $db->setQuery($query);
    
        $result = $db->loadResult(); 
    
        return $result;
    } // End function getChipsAmount()
    
    /**
     * Get billing user account object.
     * 
     * @return UserAccount
     */
    protected function _getUserAccount () {
        
        return LookBetBillingHelper::getUserAccount();
    } // End function _getUserAccount()
    
    /**
     * Get default currency name.
     * 
     * @return string
     */
    public function getDefaultCurrency () {
    
        return LookBetBillingHelper::getDefaultCurrency();
    } // End function getDefaultCurrency()
    
    /**
     * Get current user account balance.
     * 
     * @return numeric
     */
    public function getUserBalance () {
    
        return LookBetBillingHelper::getUserBalance();
    } // End function getUserBalance()
    
    /**
     * Get the array of winners'/leaders' votes with images.
     *
     * @return array
     */
    public function & getLeaderGallery () {
    
        $result = array();

        $jinput = JFactory::getApplication()->input;

        $idContract = $jinput->getInt('id_contract');
        
        JLoader::import('helper', JPATH_COMPONENT_ADMINISTRATOR);
        $result = LookBetHelper::getStandings($idContract, true);

        return $result;
    } // End function getLeaderGallery()
    
    /**
     * Get data to display given user image data
     *
     * @param integer $idUser User ID. Select user to display image data for
     * @param integer $idContract Contract ID
     * @return object
     */
    public function getUserImageData ($idUser, $idContract) {
    
        JLoader::import('helper', JPATH_COMPONENT_ADMINISTRATOR);
    
        if (!is_null(LookBetHelper::getUserContractImage($idUser, $idContract))) {
            $result = new JObject();
            
            $result->set('id', LookBetHelper::getUserContractImage($idUser,
                                                                   $idContract));
            $result->set('id_contract', $idContract);
            $result->set('id_user', $idUser);
            $result->set('num_hits', LookBetHelper::getUserScore($idUser,
                                                                 $idContract));
            $result->set('results_total',
                         LookBetHelper::getContractResultsCount($idContract));
            $result->set('position',
                         LookBetHelper::getUserPosition($idUser, $idContract));
        } else {
            $result = null;
        }
    
        return $result;
    } // End function getUserImageData()
    
    /**
     * Get data to display current user image
     *
     * @param integer $idContract Contract ID
     * @return object
     */
    public function getMyImageData ($idContract) {
        
        $user = JFactory::getUser();
    
        $result = $this->getUserImageData($user->id, $idContract);
    
        return $result;
    } // End function getMyImageData()
    
    /**
     * Get data to display contract owner image
     *
     * @param integer $idContract Contract ID
     * @return object
     */
    public function getOwnerImageData ($idContract) {
    
        $contract = $this->getContract();
        $result = $this->getUserImageData($contract->id_owner, $idContract);
    
        return $result;
    } // End function getOwnerImageData()
    
    /**
     * Get voting statistics for current contract.
     * 
     * @return array
     */
    public function getStats () {

        $result = null;

        $db = JFactory::getDBO();
        $application = JFactory::getApplication();
        $jinput = $application->input;

        $idContract = $jinput->getInt('id_contract');
        
        // Reset position counter
        $query = "SET @stats_pos := 0";
        $db->setQuery($query);
        $db->query();

        $query = "SELECT @stats_pos := (@stats_pos + 1) `position`, sg.* FROM (
SELECT s.id_contract, s.score, c.user_count, qc.num_questions FROM (SELECT q.id_contract, v.id_user, SUM(IF(v.forecast = q.event_result, 1, 0)) score
FROM #__lookbet_votes v
LEFT JOIN #__lookbet_questions q ON v.id_question = q.id
WHERE q.id_contract = '$idContract'
GROUP BY id_user
ORDER BY SUM(IF(v.forecast = q.event_result, 1, 0)) DESC) s
LEFT JOIN (
SELECT cs.score, COUNT(*) user_count FROM (
SELECT csq.id_contract, csv.id_user, SUM(IF(csv.forecast = csq.event_result, 1, 0)) score
FROM #__lookbet_votes csv
LEFT JOIN #__lookbet_questions csq ON csv.id_question = csq.id
WHERE csq.id_contract = '$idContract'
GROUP BY id_user) cs GROUP BY cs.score) c
ON s.score = c.score
LEFT JOIN (
SELECT id_contract, COUNT(*) num_questions FROM #__lookbet_questions WHERE id_contract = '$idContract') qc
ON s.id_contract = qc.id_contract
GROUP BY s.score
ORDER BY s.score DESC) sg";
        $db->setQuery($query);
        $results = $db->loadObjectList();
        
        $num_questions = (int)$results[0]->num_questions;
        $id_contract = $results[0]->id_contract;
        
        $aStat = array();
        if ($num_questions && $num_questions > 0) {
            //create, not filling, array of stats
            for ($i = $num_questions; $i >= 0; $i--) {
                $aStat[$i] = array(
                    'position' => '',
                    'id_contract' => $id_contract,
                    'score' => $i,
                    'user_count' => '0',
                    'num_questions' => $num_questions,
                );
            }
            //insert results to array of stats
            foreach ($results as $result) {
                $score = $result->score;
                $aStat[$score]['position'] = $result->position;
                $aStat[$score]['user_count'] = $result->user_count;
            }
        }
        return $aStat;
    } // End function getStats()
    
    /**
     * Get results for contract owner.
     * 
     * @return array
     */
    public function getOwnerRated () {
    
        $result = null;
    
        $result = $this->getRated('owner', 1);
    
        return $result;
    } // End function getOwnerRated()
    
    /**
     * Get results for current user.
     * 
     * @return array
     */
    public function getMyRated () {
    
        $result = null;
    
        $result = $this->getRated('my', 1);
    
        return $result;
    } // End function getMyRated()
    
    /**
     * Get the list of rated user votes.
     * 
     * @return array
     */
    public function getRated ($type, $limit = null) {
        
        JLoader::import('helper', JPATH_COMPONENT_ADMINISTRATOR);
    
        $result = null;
        $limitString = '';
        $whereString = '';
        $havingString = '';
        
        $jinput = JFactory::getApplication()->input;
        $idContract = $jinput->getInt('id_contract');
        //$usernames = array();
        //$userscores = array();
        $db = JFactory::getDBO();
        $user = JFactory::getUser();
            
        // Inintialise database variables
        // @todo Query as string! Possible compatibility issue!
        $db->setQuery('SET @pos := 0');
        $db->execute();
        // @todo Query as string! Possible compatibility issue!
        $db->setQuery('SET @prev_score := NULL');
        $db->execute();
        
        switch (strtolower($type)) {
            case 'my':
                if ($user->id) {
                    $havingString = "HAVING `id_user` = {$user->id}";
                } else {
                    $havingString = "HAVING 0 = 1"; // Return empty result
                }
                break;
            case 'owner':
                $idOwner = LookBetHelper::getContractOwner($idContract);
                $havingString = "HAVING `id_user` = $idOwner";
                break;
            case 'top':
            default:
                break;
        }
        
        if (!$limit) {
            $limitString = " LIMIT $limit";
        }
        
        // @todo Query as string! Possible compatibility issue!
        $query = "SELECT * FROM (SELECT *, IF(@prev_score <> `score` OR @prev_score IS NULL, @pos := @pos + 1, @pos) AS `position`, @prev_score := `score` FROM
(SELECT `v`.`id_user`, `u`.`name` `user_name`, SUM(IF(v.forecast = q.event_result, 1, 0)) `score` FROM `#__lookbet_votes` AS `v` INNER JOIN `#__lookbet_questions` AS `q` ON (`v`.`id_question` = `q`.`id`) LEFT JOIN `#__users` AS `u` ON (`v`.`id_user` = `u`.`id`) WHERE `q`.`id_contract` = $idContract GROUP BY `id_user` ORDER BY `score` DESC, `v`.`id`$whereString) `sc`) `scr` $havingString$limitString";
        
        $db->setQuery($query);

        $result = $db->loadAssocList('id_user');
    
        return $result;
    } // End function getRated()
    
    /**
     * Get the list of top-rated users for given contract.
     *
     * @return array
     */
    public function getTopRated () {
        
        JLoader::import('classes.params', JPATH_COMPONENT_ADMINISTRATOR);
        
        $params = LookBetParams::getInstance();

        $result = $this->getRated('top', $params->get('toplist_size'));

        return $result;
    } // End function getTopRated()
    
    /**
     * Get voting data for given user and contract.
     * 
     * @param integer $idContract contract ID
     * @param integer $idUser user ID
     * @return array
     */
    public function getUserVotes ($idContract, $idUser) {

        static $result = array();
        
        if (!isset($result[$idUser][$idContract])) {
            $db = JFactory::getDBO();
    
            $query = "SELECT q.*, v.id_user, v.forecast, IF (q.event_result = v.forecast, '1', '0') score FROM #__lookbet_questions q LEFT JOIN (SELECT * FROM #__lookbet_votes WHERE id_user = '$idUser') v ON q.id = v.id_question WHERE q.id_contract = '$idContract' ORDER BY q.event_date, q.id";
            $db->setQuery($query);
            $result[$idUser][$idContract] = $db->loadObjectList();
        }

        return $result[$idUser][$idContract];
    } // End function getUserVotes()
    
    /**
     * Save event results.
     *
     * @return boolean false on error
     */
    public function saveRes ($data) {

        $user = JFactory::getUser();
  
        $db = $this->getDbo();
  
        $query = "UPDATE `#__lookbet_questions` AS `q` JOIN `#__lookbet_contracts` AS `c`
            ON `q`.`id_contract` = `c`.`id`
            SET `q`.`event_result` = '" . $db->escape($data->event_result) . "'
            WHERE `c`.`id_owner` = " . $user->id . " AND `q`.`id` = " . $data->id . "
        ";
  
        $db->setQuery($query);
  
        $db->execute();

    } // End function saveRes()

    /**
     * Save contract data.
     *
     * @param array $data form data from request
     * @return boolean false on error
     */
    function store ($data) {

        // Where is the table? (Teacher often asked us about this matter long time ago.)

        JModelLegacy::addTablePath(JPATH_ADMINISTRATOR . '/components/com_lookbet/tables');

        $row = $this->getTable('contracts');

        if (!$row->bind($data)) {
            return false;
        }

        if (!$row->check()) {
            return false;
        }

        if (!$row->store(true)) {
            return false;
        }

        // We need last insert ID to perform wizard-style questions handling
        return $row->id;

    } // function store()

    /**
     * Load contract data.
     *
     * @return JObject
     */
    public function getContract () {

        static $result;

        JLoader::import('classes.params', JPATH_COMPONENT_ADMINISTRATOR);
        
        $params = LookBetParams::getInstance();

        if (!isset($result)) {
            $db = JFactory::getDbo();
            $application = JFactory::getApplication();
            $jinput = $application->input;
    
            $idContract = $jinput->getInt('id_contract');
    
            // @todo Query as string! Possible compatibility issue!
            $query = "SELECT c.*, u.name owner, {$params->get('default_contract_price')} price FROM #__lookbet_contracts c LEFT JOIN #__users u ON c.id_owner = u.id WHERE c.id = '$idContract'";
            $db->setQuery($query);
    
            $result = $db->loadObject();
        }

        return $result;
    } // End function getContract()

    /**
     * Load questions list.
     *
     * @return array
     */
    public function getQuestions () {

        static $result;
        
        if (!isset($result)) {
            JLoader::import('helpers.query', JPATH_COMPONENT_ADMINISTRATOR);
    
            $db = JFactory::getDBO();
            $application = JFactory::getApplication();
            $jinput = $application->input;
    
            $idContract = $jinput->getInt('id_contract');
    
            $query = LookBetQueryHelper::selectQuestions($idContract);
            $db->setQuery($query);
            $result = $db->loadObjectList();
        }

        return $result;
    } // End function getQuestions()

    public function getVotes() {

        $application = JFactory::getApplication();
        $jinput = $application->input;

        $idContract = $jinput->getInt('id_contract');
        $idUser = JFactory::getUser()->id;
        $result = $this->getUserVotes($idContract, $idUser);

        return $result;
    }
}//class
