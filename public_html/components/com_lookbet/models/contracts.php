<?php
/**
 * @package    LookBet
 * @subpackage Models
 * @version    1.00.0008 $Id: contracts.php 87 2014-08-10 19:07:42Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 19-Apr-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


jimport('joomla.application.component.model');

/**
 * Vote model.
 *
 * @package    LookBet
 * @subpackage Models
 */
class LookBetModelContracts extends JModelLegacy
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        // Additional work
        $foo = 'Bar';

        parent::__construct();
    }//function

    /**
     * Save user forecast
     *
     * @return boolean false on error
     */
    public function saveVote ($vote) {

        $user = JFactory::getUser();
  
        $db = $this->getDbo();
  
        $query = "INSERT INTO `#__lookbet_votes` SET
    `forecast` = '" . $vote->forecast . "',
    `id_question` = " . $vote->id_question . ",
    `id_user` = " . $user->id .
    " ON DUPLICATE KEY UPDATE 
    `forecast` = '{$vote->forecast}'";
  
        $db->setQuery($query);
  
        $db->execute();
        
        // Previous query does both inserting and updating so we can't use
        // LAST_INSERT_ID here.
      
        $query = "SELECT `id` FROM `#__lookbet_votes`
          WHERE `id_user` = " . $user->id . " AND `id_question` = " . $vote->id_question . "
        ";
        
        // Can do it because (id_question, id_user) is unique key.
        
        $db->setQuery($query);
        
        return $db->loadResult();

    } // End function saveVote()

    /**
     * Get event dates for given contract.
     *
     * @return array
     */
    public function getDates ($idContract) {

        $db = JFactory::getDBO();

        $query = "SELECT id_contract, event_date FROM #__lookbet_questions WHERE id_contract = '$idContract' GROUP BY id_contract, event_date";
        $db->setQuery($query);
        $result = $db->loadObjectList();

        return $result;
    } // End function getTypes()

    /**
     * Get events for given contract.
     *
     * @return array
     */
    public function getEvents ($idContract) {

        $db = JFactory::getDBO();

        $query = "SELECT id_contract, event_title FROM #__lookbet_questions WHERE id_contract = '$idContract' GROUP BY id_contract, event_title";
        $db->setQuery($query);
        $result = $db->loadObjectList();

        return $result;
    } // End function getTypes()

    /**
     * Get event types for given contract.
     *
     * @return array
     */
    public function getTypes ($idContract) {

        $db = JFactory::getDBO();

        $query = "SELECT id_contract, event_type FROM #__lookbet_questions WHERE id_contract = '$idContract' GROUP BY id_contract, event_type";
        $db->setQuery($query);
        $result = $db->loadObjectList();

        return $result;
    } // End function getTypes()

    public function getItems() {
        
        JLoader::import('classes.params', JPATH_COMPONENT_ADMINISTRATOR);
        JLoader::import('helper', JPATH_COMPONENT_ADMINISTRATOR);

        $db = JFactory::getDBO();
        $params = LookBetParams::getInstance();
        $user = JFactory::getUser();

        $verificationPeriod =  $params->get('results_verification_period');
        // @todo Query as string! Possible compatibility issue!
        $query = "SELECT c.*, q.*, IFNULL(n.num_votes, 0) num_votes, IFNULL(n.num_votes, 0) * q.price reward, 
            IF(q.first_event_date > NOW(), 'vote', IF((q.first_event_date <= NOW() AND DATE_ADD(q.last_event_date, INTERVAL $verificationPeriod DAY) >= NOW()), 'active', 'stopped')) `status`, 
            IF(q.first_event_date > NOW(), '1', IF((q.first_event_date <= NOW() AND DATE_ADD(q.last_event_date, INTERVAL $verificationPeriod DAY) >= NOW()), '2', '3')) `status_code` 
            FROM #__lookbet_contracts c 
            LEFT JOIN (SELECT id_contract, COUNT(*) num_questions, COUNT(event_result) num_results, COUNT(event_result) my_results_total, 1 price, 0 reward, MIN(event_date) first_event_date, MAX(event_date) last_event_date  FROM #__lookbet_questions GROUP BY id_contract) q ON c.id = q.id_contract 
            LEFT JOIN (SELECT id_contract, COUNT(*) num_votes FROM (SELECT id_contract, id_user FROM #__lookbet_votes nvv LEFT JOIN #__lookbet_questions nvq ON nvv.id_question = nvq.id GROUP BY id_contract, id_user) nv GROUP BY id_contract) n ON c.id = n.id_contract 
            ORDER BY `c`.`id` DESC
            LIMIT 0 , 3";
        $db->setQuery($query);
        $result = $db->loadObjectList();
        
        foreach ($result as &$curRow) {
            if ($user->id) {    
                $curRow->my_position = LookBetHelper::getUserPosition($user->id,
                                                                      $curRow->id);
                $curRow->my_score = LookBetHelper::getUserScore($user->id,
                                                                $curRow->id);
            } else {
                $curRow->my_position = null;
                $curRow->my_score = null;
            }
        }

        return $result;
    }
}//class
