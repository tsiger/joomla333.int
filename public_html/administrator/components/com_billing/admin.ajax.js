function getXmlHttp(){
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}

function UpdateCashout(paysys_id)
{
    var req = getXmlHttp()  
    req.onreadystatechange = function() {  
        if (req.readyState == 4) { 
            if(req.status == 200) { 
				var newElement = document.getElementById('checkout');
				newElement.innerHTML = req.responseText;
            }
        }
    }
 
	req.open('GET', '/administrator/components/com_billing/admin.ajax.php?task=updatecashout&paysys_id=' + paysys_id, true);  
    req.send(null);  // отослать запрос
}