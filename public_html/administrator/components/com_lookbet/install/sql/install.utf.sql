CREATE TABLE IF NOT EXISTS `#__lookbet_challenges` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `challenge_type` varchar(255) NOT NULL COMMENT 'Challenge type',
  `id_object` int(11) unsigned NOT NULL COMMENT 'The ID of challenge object of given challenge type',
  `id_group` int(10) unsigned NOT NULL COMMENT 'Linked GroupJive group ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lookbet challenges list';

CREATE TABLE IF NOT EXISTS `#__lookbet_challenge_visit` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL COMMENT 'Link URL',
  `hits` int(11) unsigned NOT NULL COMMENT 'Hits counter',
  `link_type` varchar(255) NOT NULL COMMENT 'Link type description',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lookbet Visit Link challenges';

CREATE TABLE IF NOT EXISTS `#__lookbet_contracts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_owner` int(11) unsigned DEFAULT NULL COMMENT 'RESERVED. Owner ID',
  `description` varchar(250) NOT NULL COMMENT 'Contract description',
  `date_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Contract creation date',
  `is_paid` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__lookbet_event_types` (
  `event_type` varchar(50) NOT NULL,
  PRIMARY KEY (`event_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Event types list';

CREATE TABLE IF NOT EXISTS `#__lookbet_group_properties` (
  `id` int(11) NOT NULL COMMENT 'Related group ID',
  `points` decimal(10,2) NOT NULL COMMENT 'Amount of points assigned to group',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='LookBet-specific Group Jive groups properties';

CREATE TABLE IF NOT EXISTS `#__lookbet_locations` (
  `location` varchar(50) NOT NULL,
  PRIMARY KEY (`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Event types list';

CREATE TABLE IF NOT EXISTS `#__lookbet_questions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_contract` int(11) unsigned DEFAULT NULL COMMENT 'RESERVED',
  `event_type` varchar(50) NOT NULL COMMENT 'Event type code name',
  `location` varchar(50) NOT NULL COMMENT 'Event location code name',
  `event_date` date NOT NULL COMMENT 'Event date',
  `event_title` varchar(250) NOT NULL COMMENT 'Event title',
  `part1` varchar(10) NOT NULL COMMENT 'Event participant # 1',
  `part2` varchar(10) NOT NULL COMMENT 'Event participant # 2',
  `has_draw` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Is draw possible in this event?',
  `event_result` varchar(10) NOT NULL COMMENT 'Actual event result',
  `has_image` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Does this question allow image upload',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

CREATE TABLE IF NOT EXISTS `#__lookbet_user_challenges` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL COMMENT 'User ID',
  `id_challenge` int(11) unsigned NOT NULL COMMENT 'Related challenge ID',
  `is_complete` bit(1) NOT NULL DEFAULT b'0' COMMENT 'Challenge completion flag',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index 4` (`id_user`,`id_challenge`),
  KEY `FK_#__lookbet_user_challenges_#__lookbet_challenges` (`id_challenge`),
  CONSTRAINT `FK_#__lookbet_user_challenges_#__lookbet_challenges` FOREIGN KEY (`id_challenge`) REFERENCES `#__lookbet_challenges` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_#__lookbet_user_challenges_#__users` FOREIGN KEY (`id_user`) REFERENCES `#__users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='User challenges status records';

CREATE TABLE IF NOT EXISTS `#__lookbet_user_contracts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `id_contract` int(11) unsigned NOT NULL DEFAULT '0',
  `bet` float(10,2) unsigned DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_user_id_contract` (`id_user`,`id_contract`),
  KEY `FK_#__lookbet_user_contracts_#__lookbet_contracts` (`id_contract`),
  CONSTRAINT `FK_#__lookbet_user_contracts_#__lookbet_contracts` FOREIGN KEY (`id_contract`) REFERENCES `#__lookbet_contracts` (`id`),
  CONSTRAINT `FK_#__lookbet_user_contracts_#__users` FOREIGN KEY (`id_user`) REFERENCES `#__users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__lookbet_user_properties` (
  `id` int(11) NOT NULL,
  `points` decimal(10,2) NOT NULL COMMENT 'Amount of points assigned to group',
  PRIMARY KEY (`id`),
  CONSTRAINT `FK__#__users` FOREIGN KEY (`id`) REFERENCES `#__users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='LookBet-specific user properties';

CREATE TABLE IF NOT EXISTS `#__lookbet_votes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_question` int(11) unsigned DEFAULT NULL COMMENT 'RESERVED',
  `id_user` int(11) unsigned DEFAULT NULL COMMENT 'RESERVED',
  `forecast` varchar(10) NOT NULL COMMENT 'User selected value',
  PRIMARY KEY (`id`),
  UNIQUE KEY `IX_ID_QUESTION_ID_USER` (`id_question`,`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
