<?php
/**
 * @package    LookBet
 * @subpackage Tables
 * @version    1.00.0001 $Id: questions.php 31 2014-05-08 10:43:52Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 08-May-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


/**
 * Class for table lookbet_questions.
 */
class TableQuestions extends JTable
{
	/**
	 * @var Database object
	 */
	var $db = null;

   /**
    * Constructor.
    *
    * @param object $_db Database connector object.
    */
    public function __construct(&$_db)
    {
        parent::__construct('#__lookbet_questions', 'id', $_db);
        $this->db = $_db;
    }//function
}//class
