<?php
/**
 * @package    LookBet
 * @subpackage Tables
 * @version    1.00.0001 $Id: contracts.php 22 2014-04-30 21:39:51Z dw.ilya $
 * @author     Created on 19-Apr-2014
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


/**
 * Class for table lookbet_contracts.
 */
class TableContracts extends JTable
{
	/**
	 * @var Database object
	 */
	var $db = null;

   /**
    * Constructor.
    *
    * @param object $_db Database connector object.
    */
    public function __construct(&$_db)
    {
        parent::__construct('#__lookbet_contracts', 'id', $_db);
        $this->db = $_db;
    }//function
}//class
