<?php
/**
 * @package    LookBet
 * @subpackage Tables
 * @version    3.00.0001 $Id: usercontracts.php 105 2014-11-11 17:30:18Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 11-Nov-2014
 * @since      3.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


/**
 * Class for table lookbet_user_contracts.
 */
class TableUsercontracts extends JTable {
    
	/**
	 * @var Database object
	 */
	var $db = null;

   /**
    * Constructor.
    *
    * @param object $_db Database connector object.
    */
    public function __construct(&$_db)
    {
        parent::__construct('#__lookbet_user_contracts', 'id', $_db);
        $this->db = $_db;
    }//function
}//class
