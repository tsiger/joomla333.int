<?php
/**
 * LookBet-specific user properties.
 *
 * @package    LookBet
 * @subpackage Tables
 * @version    2.00.0001 $Id: userproperties.php 95 2014-10-09 13:18:11Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @author     Created on 06-Oct-2014
 * @license    GNU/GPL
 * @since      2.00
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


/**
 * Class for table lookbet_user_properties.
 */
class TableUserProperties extends JTable
{
	/**
	 * @var Database object
	 */
	var $db = null;



   /**
    * Insert row for a given user.
    *
    * @param integer $id User ID
    * @return boolean false on error
    */
   public function insert ($id) {
   
    $result = null;
   
    $query = $this->db->getQuery(true);
    $query
        ->insert($this->getTableName())
        ->set("id = $id");
    $this->db->setQuery($query);
    $this->db->execute();
    
    if (!$this->load($id)) {
        // @todo ERROR: Cannot load data
        return false;
    }
   
    return $result;
   } // End function insert()
   
   /**
    * Constructor.
    *
    * @param object $_db Database connector object.
    */
    public function __construct(&$_db)
    {
        parent::__construct('#__lookbet_user_properties', 'id', $_db);
        $this->db = $_db;
    }//function
}//class
