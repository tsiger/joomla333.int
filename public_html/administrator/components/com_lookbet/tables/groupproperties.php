<?php
/**
 * Group Jive extra properties to user with LookBet component
 * 
 * @package    LookBet
 * @subpackage Tables
 * @version    2.00.0002 $Id: groupproperties.php 104 2014-10-30 22:23:18Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @author     Created on 09-Oct-2014
 * @license    GNU/GPL
 * @since      2.00
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


/**
 * Class for table lookbet_group_properties.
 */
class TableGroupProperties extends JTable
{
	/**
	 * @var Database object
	 */
	var $db = null;
    const BILLING_ACTION_CODE = 'challenge_visit';



    /**
     * Move points from group account to specific user account.
     *
     * @param decimal $amount Number of points to pay
     * @param integer $idUser Recipient user ID. Current user, if omitted
     * @return mixed number of points moved or false on error
     */
    public function payToUser ($amount, $idUser = null) {
        
        $result = null;
        
        // Load billing API
        JLoader::import('components.com_billing.userpoints', JPATH_BASE);
        
        if ($this->id) {
            if (!UserPointsEnabled()) {
                // @todo ERROR: User Points disabled
                return false;
            }
            if ($this->points >= $amount) {
                if (is_null($idUser)) {
                    $idUser = JFactory::getUser()->id;
                }
                
                if (is_null($idUser)) {
                    // @todo ERROR: No user selected
                    return false;
                }
                
                $userProperties = $this->getInstance('UserProperties', 'Table');
                //if (!) {
                //    // @todo ERROR: Cannot load user account data
                //    return false;
                //}
                
                $userProperties->load($idUser);
                
                $this->points -= $amount;
                if (!$this->store()) {
                    // @todo ERROR: Error saving group account data
                    return false;
                }
                if (!$userProperties->id) {
                    $userProperties->insert($idUser);
                }
                $idAction = GetActionIdByCode($this::BILLING_ACTION_CODE);
                AddUserPoints($idUser, $idAction, 'Visit site', $amount);
                if (!$userProperties->store()) {
                    // @todo ERROR: Error saving user account data
                    // Return points to group account
                    $this->points += $amount;
                    if (!$this->store()) {
                        // @todo ERROR: Cannot return points to group account
                        return false;
                    }
                    return false;
                }
            } else {
                // @todo ERROR: Insufficient points on group account
                return false;
            }
        } else {
            // @todo ERROR: Group data not loaded
            return false;
        }
        
     return $amount;
    } // End function payToUser()
    
    /**
     * Constructor.
     *
     * @param object $_db Database connector object.
     */
     public function __construct(&$_db)
     {
         parent::__construct('#__lookbet_group_properties', 'id', $_db);
         $this->db = $_db;
     }//function
}//class
