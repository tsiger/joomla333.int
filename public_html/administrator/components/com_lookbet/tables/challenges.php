<?php
/**
 * @package    LookBet
 * @subpackage Tables
 * @version    2.00.0001 $Id: challenges.php 93 2014-10-06 22:19:17Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @author     Created on 06-Oct-2014
 * @license    GNU/GPL
 * @since      2.00
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


/**
 * Class for table lookbet_challenges.
 */
class TableChallenges extends JTable
{
	/**
	 * @var Database object
	 */
	var $db = null;



   /**
    * Constructor.
    *
    * @param object $_db Database connector object.
    */
    public function __construct(&$_db)
    {
        parent::__construct('#__lookbet_challenges', 'id', $_db);
        $this->db = $_db;
    }//function
}//class
