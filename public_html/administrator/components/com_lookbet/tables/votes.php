<?php
/**
 * @package    LookBet
 * @subpackage Tables
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @author     Created on 19-Apr-2014
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


/**
 * Class for table lookbet_votes.
 */
class TableVotes extends JTable
{
	/**
	 * @var Database object
	 */
	var $db = null;



   /**
    * Constructor.
    *
    * @param object $_db Database connector object.
    */
    public function __construct(&$_db)
    {
        parent::__construct('#__lookbet_votes', 'id', $_db);
        $this->db = $_db;
    }//function
}//class
