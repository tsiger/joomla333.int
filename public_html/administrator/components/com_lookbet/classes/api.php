<?php
/**
 * LookBet API.
 *
 * @example     JLoader::import('components.com_lookbet.classes.api', JPATH_ADMINISTRATOR);
 *              $api = LookbetApi::getInstance();
 *              
 * @package     LookBet
 * @subpackage  Classes
 * @version     2.00.0009 $Id: api.php 103 2014-10-22 11:24:03Z dw.ilya $
 * @author      Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright   2014 Factory.DocWriter.Ru
 * @author      Created on 06-Jun-2014
 * @license     GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');

class LookBetApi {
    
    function __construct () {
        
        if (!defined('LOOKBET_PATH_COMPONENT_ADMINISTRATOR')) {
            define('LOOKBET_PATH_COMPONENT_ADMINISTRATOR',  JPATH_ADMINISTRATOR
                   . '/components/com_lookbet');
        }
        if (!defined('LOOKBET_PATH_COMPONENT_SITE')) {
            define('LOOKBET_PATH_COMPONENT_SITE',  JPATH_SITE
                   . '/components/com_lookbet');
        }
        JHtml::addIncludePath(LOOKBET_PATH_COMPONENT_ADMINISTRATOR
                              . '/classes/html');
        JTable::addIncludePath(LOOKBET_PATH_COMPONENT_ADMINISTRATOR
                              . '/tables');
        
        $lang = JFactory::getLanguage();
        $lang->load('com_lookbet')
            || $lang->load('com_lookbet', LOOKBET_PATH_COMPONENT_SITE);
    }
    
    /**
     * Load the challenges list for given group.
     *
     * @param   integer $idGroup parent group ID
     * @return  array
     */
    public function getChallenges ($idGroup, $showAll = false) { 
    
        $result = null;
        
        JLoader::import('classes.challenges',
                        LOOKBET_PATH_COMPONENT_ADMINISTRATOR);
        $result = LookBetChallenges::getInstance($idGroup, $showAll);
    
        return $result;
    } // End function getChallenges()
    
    /**
     * Get common URI.
     *
     * Append method arguments as needed.
     *
     * @param string $type URI type to return
     * @return string
     */
    public function getUri ($type) {
    
        $result = null;
        
        JLoader::import('helpers.uri', LOOKBET_PATH_COMPONENT_ADMINISTRATOR);
        
        // Get args list
        $args = func_get_args();
        // Remove function name from arguments
        array_shift($args);
    
        $className = 'LookBetUriHelper';
        $result = call_user_func_array(array($className, $type), $args);
    
        return $result;
    } // End function getUri()
    
    /**
     * Get image thumbnail URI creating it if not already exists.
     * 
     * @param string $name Image file name without path and extension
     * @return string
     */
    public function getThumb ($name) {
        
        JLoader::import('helpers.image', LOOKBET_PATH_COMPONENT_ADMINISTRATOR);
    
        $result = LookBetImageHelper::getThumb($name);
        
        return $result;
    } // End function getThumb()
    
    /**
     * Get gallery image URI.
     *
     * @param string $name Image file name without path and extension
     * @param boolean $path If true, return local path. If false, return full URI.
     * @return string
     */
    public function getImage ($name, $path = false) {
    
        JLoader::import('helpers.image', LOOKBET_PATH_COMPONENT_ADMINISTRATOR);
        // Load broken dependancy for LookBetImageHelper class
        JLoader::import('classes.common.image',
                        LOOKBET_PATH_COMPONENT_ADMINISTRATOR); 
    
        $result = LookBetImageHelper::getImage($name, $path);
    
        return $result;
    } // End function getImage()
    
    /**
     * Load component stylesheet.
     * 
     * @return void
     */
    public function loadStylesheet () {
    
        $result = null;
    
        $app = JFactory::getApplication();
        
        if ($app->isSite()) {
            $root = JURI::root() . '/components/com_lookbet';
        } else {
            $root = JURI::root() . '/administrator/components/com_lookbet';
        }
        $path = "$root/assets/css/lookbet.css";
        JHtml::_('stylesheet', $path);
    
        return $result;
    } // End function loadStylesheet()
    
    /**
     * Get the array of user votes with images.
     *
     * Wrapper for LookBetModelGallery::getUserImages() method.
     *
     * @param integer $idUser User ID
     * @return array
     */
    public function & getUserImages ($idUser) {
    
        $result = null;
    
        JModelLegacy::addIncludePath(LOOKBET_PATH_COMPONENT_SITE . '/models');
        $model = JModelLegacy::getInstance('Gallery', 'LookBetModel');
        
        $result = $model->getUserImages($idUser);
    
        return $result;
    } // End function getUserImages()
    
    /**
     * Load LookBet language file.
     * 
     * @return string
     */
    function loadLanguage () {
    
        $result = null;
        
        $lang = JFactory::getLanguage();
        $comName = 'com_lookbet';
        $lang->load($comName, LOOKBET_PATH_COMPONENT_SITE);
    
        return $result;
    } // End function loadLanguage()
    
    /**
     * Import LookBet helper.
     *
     * @param string $path  Helper path from the helpers directory root in
     *                      dot-separated format. Load default helper if omitted
     * @return void
     */
    function importHelper ($path = null) {
    
        if (!is_null($path)) {
            JLoader::import("helpers.$path", JPATH_ADMINISTRATOR
                            . '/components/com_lookbet');
        } else {
            JLoader::import("helper", JPATH_ADMINISTRATOR
                            . '/components/com_lookbet');
        }
    
    } // End function importHelper()
    
    /**
     * Return LookBetApi class instance only creating it if not already exists.
     *
     * @return LookBetApi
     */
    static function & getInstance () {
        
        static $result;
        
        if (!isset($result)) {
            $result = new self();
        }
        
        return $result;
    }
}