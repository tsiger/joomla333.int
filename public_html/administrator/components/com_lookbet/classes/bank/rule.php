<?php
/**
 * LookBet base bank distribution rule.
 *
 * PHP 5.3 required!
 *
 * @package     LookBet
 * @subpackage  Classes
 * @version     3.00.0001 $Id: rule.php 106 2014-11-13 23:16:57Z dw.ilya $
 * @copyright   2014
 * @author      Created on 13-Nov-2014
 * @license     GNU/GPL
 * @since       3.00
 */
defined('_JEXEC') or die('Restricted access');

class LookBetBankRule {
    
    protected $_conditions = null;
    
    function __construct ($conditions) {
        
        $this->_conditions = $conditions;
    }
    
    /**
     * Check if this rule is applicaple to given conditions.
     *
     * @param array $conditions Conditions set
     * @return boolean
     */
    public static function isApplicable ($conditions) {
    
        $result = false;
        
        // Re-define this in child class
    
        return $result;
    } // End function isApplicable()
    
    /**
     * Return LookBetChallenges new class instance for given group.
     *
     * @param decimal $amount Bank amount
     * @param array $conditions The set of conditions to pick up applicable rules
     * @return LookBetChallenges
     */
    static function & getInstance ($conditions) {
        
        if (static::isApplicable($conditions)) {
            $result = new static($conditions);
        } else {
            $result = false;
        }
        
        return $result;
    }
}