<?php
/**
 * LookBet default bank distribution rule.
 *
 * Distribute bank among claimers with 6 to 10 correct answers. If there are no
 * winners, move corresponding share(s) to special "house account".
 *
 * @package     LookBet
 * @subpackage  Classes
 * @version     3.03.0002 $Id: default.php 106 2014-11-13 23:16:57Z dw.ilya $
 * @copyright   2014
 * @author      Created on 13-Nov-2014
 * @license     GNU/GPL
 * @since       3.00
 */
defined('_JEXEC') or die('Restricted access');

JLoader::import('classes.bank.rule', JPATH_COMPONENT_ADMINISTRATOR);

class LookBetBankRuleDefault extends LookBetBankRule {
    
    protected $_shares = array();
    
    function __construct ($conditions) {

        parent::__construct($conditions);
    }
    
    function __call ($name, $arguments) {
        
echo "<p>LookBetBankRuleDefault::$name() NOT EXISTS!!!</p>"; ##
    }
    
    /**
     * Get the ID of user representing house account.
     * 
     * @return integer
     */
    protected function _getHouseAccountId () {
    
        static $result;
    
        if (!isset($result)) {
            JLoader::import('classes.params', JPATH_COMPONENT_ADMINISTRATOR);
            $params = LookBetParams::getInstance();
            
            $houseUser = JFactory::getUser($params->get('house_account'));
            $result = $houseUser->id;
        }
    
        return $result;
    } // End function _getHouseAccountId()
    
    /**
     * Load actual share values.
     * 
     * @return void
     */
    protected function _loadShares ($amount) {
    
        $result = null;
        
        JLoader::import('classes.params', JPATH_COMPONENT_ADMINISTRATOR);
        
        $db = JFactory::getDbo();
        $params = LookBetParams::getInstance();
        
        // Reset shares
        $this->_shares = array();
        
        $fee = round($amount * $params->get('contract_fee') / 100, 2);
        $prize = $amount - $fee;
    
        $idContract = $this->_conditions['id_contract'];
        
        // Load claimers list with bet values (quantifiers)
        $query = $db->getQuery(true);
        $query
            ->select(array('id_user', 'bet'))
            ->from('#__lookbet_user_contracts')
            ->where("id_contract = $idContract")
            ->where("bet > 0");
        $db->setQuery($query);
        $quantifiers = $db->loadObjectList('id_user');
        $claimerIds = array_keys($quantifiers);
        $claimersList = implode(', ', $claimerIds);
        
        // Load multipliers list
        $query = "SELECT * FROM (SELECT v.id_user, COUNT(v.id) - 5 multiplier FROM #__lookbet_votes v LEFT JOIN #__lookbet_questions q ON v.id_question = q.id WHERE q.id_contract = $idContract AND v.forecast = q.event_result AND v.id_user IN ($claimersList) GROUP BY v.id_user) c WHERE c.multiplier > 0";
        $db->setQuery($query);
        $multipliers = $db->loadObjectList('id_user');
        $winnerIds = array_keys($multipliers);
        
        // Calculate relative share values
        $sharesTotal = 0;
        foreach ($multipliers as $curKey => $curRow) {
            $this->_shares[$curKey] = $curRow->multiplier
                * $quantifiers[$curKey]->bet;
            $sharesTotal += $this->_shares[$curKey];
        }
        
        // Convert relative share values into actual gains for each claimer
        foreach ($this->_shares as $curKey => $curValue) {
            $this->_shares[$curKey] = round($prize * $curValue / $sharesTotal, 2);
        }

        // Calculate house gains
        if (sizeof($this->_shares) == 0) {
            // The whole bank goes to the house account
            $this->_shares[$this->_getHouseAccountId()] = $amount;
        } else {
            // The whole bank goes to the house account
            $this->_shares[$this->_getHouseAccountId()] = $fee;
            // @todo: Check if there are no 10 and 9 correct answers. If not, add these shares to bank account
        }

        return $result;
    } // End function _loadShares()
    
    /**
     * Move chips to winners' accounts.
     * 
     * @return void
     */
    protected function _payGains () {
    
        $result = null;
        
        JLoader::import('helpers.billing', JPATH_COMPONENT_ADMINISTRATOR);
    
        foreach ($this->_shares as $curId => $curAmount) {
            LookBetBillingHelper::payToUser($curId, $curAmount);
        }
    
        return $result;
    } // End function _payGains()
    
    /**
     * Distribute amount.
     *
     * @param numeric $amount Amount to distribute
     * @return void
     */
    public function distribute ($amount) {

        $result = null;
    
        $this->_loadShares($amount); // Calculate share values
        $this->_payGains(); // Move chips to winners' accounts
    
        return $result;
    } // End function distribute()
    
    /**
     * Check if this rule is applicaple to given conditions.
     *
     * @param array $conditions Conditions set
     * @return boolean
     */
    public static function isApplicable ($conditions) {
    
        $result = false;
        
        if ($conditions['num_questions'] == 10) {
            $result = true;
        }
    
        return $result;
    } // End function isApplicable()
}