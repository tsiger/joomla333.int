<?php
/**
 * Class for HTML link to Visit challenge item.
 *
 * @version    3.02.0004 SVN: $Id: visit.php 119 2014-12-09 00:48:45Z dw.ilya $
 * @package    LookBet
 * @subpackage HTML
 * @author     Created on 26-Jun-2014
 * @license    GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @since      File available since Release 1.00
 */
defined('_JEXEC') or die('Restricted access');

class LookBetHtmlChallengeVisit {
    
    /**
     * Get challenge link HTML code.
     *
     * @param Object    $row Challenge data item
     * @param array     $param Parameters array:
     *                      - $param[0] - show hits count
     *                      - $param[1] - inactive link
     * @return string
     */
    static function link (&$row, $param = null) {
    
        $result = '';
        
        $price = 25;
        $showHitsCount = $param[0];
        $inactiveLink = $param[1];
        
        $linkClass = '';
        
        $link = JRoute::_("index.php?option=com_lookbet&task=visit.go&id={$row->id}");
        $linkType = $row->link_type;
        $text = $row->link;
        if (!$linkType) {
            $linkType = 'VISIT_LINK';
        }
        
        if ($inactiveLink) {
            $linkClass = ' class="lb_inactive"';
        }
        
        $linkPrompt = 'VISIT_TYPE_' . strtoupper($linkType);
        
        $caption = JText::sprintf('CHALLENGE_LINK_CAPTION_S',
                                  JText::_($linkPrompt), $price);
        
        $result .= "$caption: <a href=\"$link\" target=\"_blank\"$linkClass>$text</a>";
        if ($param[0]) { // Show hits count
            // Hits count
            $result .= '. ' . JText::_('HITS') . ': ' . $row->hits;
        }
    
        return $result;
    } // End function link()
}