<?php
/**
 * Class for LookBet gallery items.
 *
 * @version    1.00.0002 SVN: $Id: gallery.php 85 2014-08-01 22:25:51Z dw.ilya $
 * @package    LookBet
 * @subpackage HTML
 * @author     Created on 15-Jul-2014
 * @license    GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @since      File available since Release 1.00
 */
defined('_JEXEC') or die('Restricted access');

class LookBetHtmlGallery {
    
    /**
     * Get gallery item for the leader HTML code.
     *
     * @param object $data Data used to display gallery item
     * @return string
     */
    static function leaderitem (&$data) {
        
        JLoader::import('classes.params', JPATH_COMPONENT_ADMINISTRATOR);
        JLoader::import('helpers.image', JPATH_COMPONENT_ADMINISTRATOR);
        JLoader::import('helpers.uri', JPATH_COMPONENT_ADMINISTRATOR);
        
        $params = LookBetParams::getInstance();

        $thumbWidth = $params->thumb_width;
        $thumbHeight = $params->thumb_height;
        
        $imagePath = LookBetImageHelper::getImage($data->id);
        $thumbPath = LookBetImageHelper::getThumb($data->id);
        $profileLink = JHtml::_('lookbethtml.cblink.link', $data->id_user);
    
        $result = "		<p>
			<a href=\"$imagePath\" target=\"_blank\"><img src=\"$thumbPath\" style=\"width: {$thumbWidth}px; height: {$thumbHeight}px;\"></a><br>
			$profileLink</p>
";
    
        return $result;
    } // End function link()
    
    /**
     * Get gallery item HTML code.
     *
     * @param object $data Data used to display gallery item
     * @return string
     */
    static function item (&$data) {
        
        JLoader::import('classes.params', JPATH_COMPONENT_ADMINISTRATOR);
        JLoader::import('helpers.image', JPATH_COMPONENT_ADMINISTRATOR);
        JLoader::import('helpers.uri', JPATH_COMPONENT_ADMINISTRATOR);
        
        $params = LookBetParams::getInstance();

        $thumbWidth = $params->thumb_width;
        $thumbHeight = $params->thumb_height;
        
        $imagePath = LookBetImageHelper::getImage($data->id);
        $thumbPath = LookBetImageHelper::getThumb($data->id);
        $profileLink = JHtml::_('lookbethtml.cblink.link', $data->id_user);
    
        $result = "		<p>
			<a href=\"$imagePath\" target=\"_blank\"><img src=\"$thumbPath\" style=\"width: {$thumbWidth}px; height: {$thumbHeight}px;\"></a><br>
			$profileLink</p>

		<p>
			{$data->num_hits}/{$data->results_total}</p>

		<p>
			# {$data->position}</p>
";
    
        return $result;
    } // End function link()
}