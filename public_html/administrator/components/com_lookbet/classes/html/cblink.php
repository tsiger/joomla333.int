<?php
/**
 * Class for HTML link to Community Builder user profile.
 *
 * @version    1.00.0001 SVN: $Id: cblink.php 77 2014-06-25 21:56:01Z dw.ilya $
 * @package    LookBet
 * @subpackage HTML
 * @author     Created on 26-Jun-2014
 * @license    GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @since      File available since Release 1.00
 */
defined('_JEXEC') or die('Restricted access');

class LookBetHtmlCblink {
    
    /**
     * Get user profile link HTML code.
     *
     * @param integer $idUser User ID
     * @return string
     */
    static function link ($idUser) {
    
        $result = '';
        
        $user = JFactory::getUser($idUser);
        $link = JRoute::_("index.php?option=com_comprofiler&task=userprofile&user=$idUser");
        $text = $user->name;
        
        $result .= "<a href=\"$link\">$text</a>";
    
        return $result;
    } // End function link()
}