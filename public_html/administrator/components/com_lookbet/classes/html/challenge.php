<?php
/**
 * Class for HTML link to challenge item.
 *
 * @version    2.00.0002 SVN: $Id: challenge.php 100 2014-10-10 13:17:01Z dw.ilya $
 * @package    LookBet
 * @subpackage HTML
 * @author     Created on 26-Jun-2014
 * @license    GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @since      File available since Release 1.00
 */
defined('_JEXEC') or die('Restricted access');

class LookBetHtmlChallenge {
    
    /**
     * Get challenge link HTML code.
     *
     * @param Object $row Challenge data item
     * @return string
     */
    static function link (&$row) {
    
        $result = '';
        
        $param = func_get_args();
        array_shift($param);
        
        $type = $row->challenge_type;
        $class = __CLASS__ . ucfirst($type);

        JLoader::import("classes.html.challenge.$type",
                        LOOKBET_PATH_COMPONENT_ADMINISTRATOR);
        $result = $class::link($row, $param);
        
        return $result;
    } // End function link()
}