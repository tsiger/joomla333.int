<?php
/**
 * Class for LookBet gallery items.
 *
 * @version    3.00.0001 SVN: $Id: contract.php 105 2014-11-11 17:30:18Z dw.ilya $
 * @package    LookBet
 * @subpackage HTML
 * @author     Created on 11-Nov-2014
 * @license    GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @since      File available since Release 3.00
 */
defined('_JEXEC') or die('Restricted access');

class LookBetHtmlContract {
    
    /**
     * Control to select the bet amount.
     *
     * @param numeric $price Contract price
     * @param string $currency Currency name
     * @param numeric $userBalance Amount available for user to pay
     * @return string
     */
    static function bet ($price, $currency, $userBalance) {
    
        $result = null;
    
        if ($userBalance >= $price) {
            $result = '<input type="checkbox" name="bet" id="bet" value="'
                . $price . '" /><label for="bet">'
                . JText::sprintf('BET_S_S_YOU_HAVE_S_S', $currency, $price,
                                 $userBalance)
                . '</label>';
        } else {
            $result = JText::_('YOU_CANNOT_BET') . ' '
                . JText::sprintf('YOU_NEED_S_S_TO_BET_YOUR_BALANCE_IS_S_S',
                                 $currency, $price, $userBalance);
        }   
    
        return $result;
    } // End function bet()
    
    /**
     * Get gallery item for the leader HTML code.
     *
     * @param object $data Data used to display gallery item
     * @return string
     */
    static function leaderitem (&$data) {
        
        JLoader::import('classes.params', JPATH_COMPONENT_ADMINISTRATOR);
        JLoader::import('helpers.image', JPATH_COMPONENT_ADMINISTRATOR);
        JLoader::import('helpers.uri', JPATH_COMPONENT_ADMINISTRATOR);
        
        $params = LookBetParams::getInstance();

        $thumbWidth = $params->thumb_width;
        $thumbHeight = $params->thumb_height;
        
        $imagePath = LookBetImageHelper::getImage($data->id);
        $thumbPath = LookBetImageHelper::getThumb($data->id);
        $profileLink = JHtml::_('lookbethtml.cblink.link', $data->id_user);
    
        $result = "		<p>
			<a href=\"$imagePath\" target=\"_blank\"><img src=\"$thumbPath\" style=\"width: {$thumbWidth}px; height: {$thumbHeight}px;\"></a><br>
			$profileLink</p>
";
    
        return $result;
    } // End function link()
    
    /**
     * Get gallery item HTML code.
     *
     * @param object $data Data used to display gallery item
     * @return string
     */
    static function item (&$data) {
        
        JLoader::import('classes.params', JPATH_COMPONENT_ADMINISTRATOR);
        JLoader::import('helpers.image', JPATH_COMPONENT_ADMINISTRATOR);
        JLoader::import('helpers.uri', JPATH_COMPONENT_ADMINISTRATOR);
        
        $params = LookBetParams::getInstance();

        $thumbWidth = $params->thumb_width;
        $thumbHeight = $params->thumb_height;
        
        $imagePath = LookBetImageHelper::getImage($data->id);
        $thumbPath = LookBetImageHelper::getThumb($data->id);
        $profileLink = JHtml::_('lookbethtml.cblink.link', $data->id_user);
    
        $result = "		<p>
			<a href=\"$imagePath\" target=\"_blank\"><img src=\"$thumbPath\" style=\"width: {$thumbWidth}px; height: {$thumbHeight}px;\"></a><br>
			$profileLink</p>

		<p>
			{$data->num_hits}/{$data->results_total}</p>

		<p>
			# {$data->position}</p>
";
    
        return $result;
    } // End function link()
}