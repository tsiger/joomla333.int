<?php
/**
 * Image processing.
 *
 * @category   Images
 * @package    DWImage
 * @author     Factory.DocWriter.Ru info@docwriter.ru
 * @copyright  2014 Factory.DocWriter.Ru
 * @license    GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @version    1.00.0005
 * @link       http://factory.docwriter.ru
 */

defined('_JEXEC') or die('Restricted access');

class DWImage {

    private $_image = null;
    private $_textPositions = array();
    private $_defaultFont = null;
    private $_defaultTextSize = 10;
    
    function __destruct() {
        
        if($this->_image) imagedestroy($this->_image);
        $this->_textPositions = array();
    }

    public function __construct () {
        
        $this->_defaultFont = JPATH_COMPONENT_SITE . '/assets/fonts/verdana.ttf';
    }
        
    /**
     * Put multiple images into the pre-set positions.
     *
     * @param array $entries    Array of entries:
     *                          array('pos_name' => 'path')
     * @return void
     */
    public function putImagesToPositions ($entries) {
    
        foreach ($entries as $curPosition => $curPath) {
            $this->putImageToPosition($curPath, $curPosition);
        }
    } // End function putTextToPositions()
    
    /**
     * Output image into the pre-set position.
     *
     * @param string $path Image path to output
     * @param string $position Pre-set position name
     * @return void
     */
    public function putImageToPosition ($path, $position) {
    
        if (is_array($this->_textPositions[$position])) {
            $this->addWatermark($path, $this->_textPositions[$position]['x'],
                                $this->_textPositions[$position]['y'], false);
        }
    }
    
    /**
     * Load image from file.
     * 
     * @return boolean false on error
     */
    public function load ($file) {
    
        $result = null;
        
        jimport('joomla.filesystem.file');
    
        // Checks if watermark file exists
        if(!JFile::exists($file)) {
            return false; // Error: image not found
        }
    
        // Gets image info (height, width, mime)
        $imageInfo = getimagesize($file);

        switch($imageInfo[2]) {
            case 1:
                // image/gif
                $this->_image  = imagecreatefromgif($file);
                break;
            case 2:
                // image/jpeg
                $this->_image  = imagecreatefromjpeg($file);
                break;
            case 3:
                // image/png
                $this->_image  = imagecreatefrompng($file);
                break;
            default:
                return false; // Error: invalid image type
                break;
        }
        
        if (!$this->_image) {
            return false;
        }
        
        $result = true;
        return $result;
    } // End function load()
    
    /**
     * Put multiple text entries into the pre-set positions.
     *
     * @param array $entries    Array of entries:
     *                          array('pos_name' => 'text')
     * @return void
     */
    public function putTextToPositions ($entries) {
    
        foreach ($entries as $curPosition => $curText) {
            $this->putTextToPosition($curText, $curPosition);
        }
    } // End function putTextToPositions()
    
    /**
     * Output text into the pre-set position.
     *
     * @param string $text Text to output
     * @param string $position Pre-set position name
     * @return void
     */
    public function putTextToPosition ($text, $position) {
    
        if (is_array($this->_textPositions[$position])) {
            $this->putText($text, $this->_textPositions[$position]['x'],
                           $this->_textPositions[$position]['y']);
        }
    
    } // End function putTextToPosition()
    
    /**
     * Set positions for text output.
     *
     * @param array $positions  Position definitions. Key contains position
     *                          name. Array values contain arrays with x and y
     *                          coordinates. E. g.
     *                          array('pos1' => array('x' => 10, 'y' => 20))
     * @return void
     */
    public function setTextPositions ($positions) {
    
        $this->_textPositions = $positions;
    } // End function setTextPositions()
    
    /**
     * Put text onto image.
     * 
     * @param string $text text
     * @param integer $x text X position
     * @param integer $y text Y position
     */
    function putText ($text, $x, $y) {
       
        imagettftext($this->_image, $this->_defaultTextSize, 0, $x + 1, $y + 1,
                     imagecolorallocate($this->_image, 255, 255, 255),
                     $this->_defaultFont, $text);
        imagettftext($this->_image, $this->_defaultTextSize, 0, $x, $y,
                     imagecolorallocate($this->_image, 0, 0, 0),
                     $this->_defaultFont, $text);
    }
    
    /**
     * Gets image from uploaded file and stores.
     * 
     * @return 
     */
    public function getFromUpload ($imageData) {

        switch($imageData['type']){
          case 'image/png':
            $this->_image = imagecreatefrompng($imageData['tmp_name']);
            break;
          case 'image/jpeg':
            $this->_image = imagecreatefromjpeg($imageData['tmp_name']);
            break;
          case 'image/gif':
            $this->_image = imagecreatefromgif($imageData['tmp_name']);
            break;
          default:
            throw new Exception("Invalid image format.");
        }
        
        if(!$this->_image) throw new Exception("Cannot read image data.");
        
    } // End function getFromUpload()
    
    /**
     * Resize image to given width and height constraining proportions.
     *
     * @param integer $width New image width
     * @param integer $height New image height
     * @return void
     */
    public function resize ($width, $height) {
    
        $originalWidth = imagesx($this->_image);
        $originalHeight = imagesy($this->_image);
        
        $ratio = $height / $originalHeight;
        
        $newWidth = $originalWidth * $ratio;
        
        $blank_image = imagecreatetruecolor($width, $height); 
             
        $white = imagecolorallocate($blank_image, 255, 255, 255);      
        imagefill($blank_image, 0, 0, $white);
        
        if($newWidth < $width) {
            imagecopyresampled(
                $blank_image, // destination image
                $this->_image, //source image
                ( ($width - $newWidth) / 2), //destination X
                0, //destination Y
                0, // source X
                0, // source Y
                $newWidth, // destination width
                $height, // destination height
                $originalWidth, // source width
                $originalHeight // source height
            );
        } else {
                imagecopyresampled(
                    $blank_image, // destination image
                    $this->_image, //source image
                    0, //destination X
                    0, //destination Y
                    ( ( ($newWidth - $width) / 2) / $ratio ), // source X
                    0, // source Y
                    $newWidth, // destination width
                    $height, // destination height
                    $originalWidth, // source width
                    $originalHeight // source height
                );
        }
        
        $this->_image = $blank_image;      
      
    } // End function resize()
    
    /**
     * Overlays watermark over image.
     *
     * @param string $path Watermark image path
     * @param integer $posX Destenation position X
     * @param integer $posY Destenation position Y
     * @param boolean $resize Resize watermark to original image size
     * @return void
     */
    public function addWatermark ($path, $posX = 0, $posY = 0, $resize = true) {

        $destWidth = null;
        $destHeight = null;
        
        $watermark = imagecreatefrompng($path);
        
        if ($resize) {
            $destWidth = imagesx($this->_image);
            $destHeight = imagesy($this->_image);
        } else {
            $destWidth = imagesx($watermark);
            $destHeight = imagesy($watermark);
        }
   
        imagecopyresampled(
            $this->_image, // destination image
            $watermark, //source image
            $posX, //destination X
            $posY, //destination Y
            0, // source X
            0, // source Y
            $destWidth, // destination width
            $destHeight, // destination height
            imagesx($watermark), // source width
            imagesy($watermark) // source height
        );
      
    } // End function addWatermark()
    
    /**
     * Save image to file.
     *
     * @param string $path Target path
     * @return void
     */
    public function save ($path) {
        
        imagejpeg($this->_image, $path);
    } // End function save()
}
