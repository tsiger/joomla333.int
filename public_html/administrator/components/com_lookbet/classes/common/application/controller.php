<?php
/**
 * Base controller for LookBet
 *
 * @version    1.00.0001 SVN: $Id: controller.php 67 2014-06-10 21:26:07Z dw.ilya $
 * @package    LookBet
 * @author     Created on 11-Jun-2014
 * @copyright  2014 Factory.DocWriter.Ru
 * @license    GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @since      File available since Release 1.00
 */

//-- No direct access
defined('_JEXEC') or die('Direct access not alowed.');

jimport('joomla.application.component.controller');

/**
 * LookBet default Controller
 *
 * @package    LookBet
 * @subpackage Controllers
 */
class LookBetBaseController extends JControllerLegacy
{
    /**
     * Check if current user is a registered user.
     * 
     * @return boolean
     */
    function isRegisteredUser () {
        
        $result = false;
        
        $user = JFactory::getUser();
        if ($user->id) {
            $result = true;
        }
        
        return $result;
    }
    
    /**
     * Get the referring page URL.
     * 
     * @return string
     */
    function getParentUrl () {
        
        $result = null;
        
        $result = JRequest::getVar('HTTP_REFERER', null, 'server');
        
        return $result;
    }
    
    /**
     * Redirect to login form and return to referring page.
     * 
     * @param $msg string Notice text displayed on redirect
     */
    function loginAndGoBack ($msg) {
        
        $this->loginRedirect($msg, $this->getParentUrl());
    }
    
    /**
     * Redirect to login form.
     * 
     * @param $msg string Notice text displayed on redirect
     * @param $returnUrl string URL to return to after login
     */
    function loginRedirect ($msg = null, $returnUrl = null) {
        
        $curUri = JURI::getInstance();
        if (is_null($returnUrl)) $returnUrl = $curUri->toString();
        $return = base64_encode($returnUrl);
        $link = JRoute::_(JURI::root() . 'index.php?option=com_users&view=login&return=' . $return);
        $this->setRedirect($link, $msg, 'notice');
    }
    
    /**
     * Method to display the view for registered users only. Unregistered users
     * are forwarded to the login form.
     *
     * @param $msg string Notice text displayed on redirect
     *
     * @access	public
     */
    function displaySecure($msg = null) {
        
        if (!$this->isRegisteredUser()) {
            $this->loginRedirect($msg);
        } else {
            parent::display();
        }
    }//function

}//class
