<?php
/**
 * LookBet bank distribution object.
 *
 * @example     JLoader::import('classes.bank', JPATH_COMPONENT_ADMINISTRATOR);
 *              $bank = LookBetBank::getInstance();
 *              
 * @package     LookBet
 * @subpackage  Classes
 * @version     3.00.0001 $Id: bank.php 106 2014-11-13 23:16:57Z dw.ilya $
 * @copyright   2014
 * @author      Created on 13-Nov-2014
 * @license     GNU/GPL
 * @since       3.00
 */
defined('_JEXEC') or die('Restricted access');

class LookBetBank {
    
    protected $_amount = 0;
    protected $_rules = array();
    protected $_rulesPath = null;
    protected $_conditions = null;
    
    function __construct ($amount, $conditions) {

        $this->_rulesPath = dirname(__FILE__) . '/bank/rules';
        $this->_amount = $amount;
        $this->_conditions = $conditions;
        $this->loadRules();
    }

    /**
     * Distribute amount via loaded rules.
     * 
     * @return void
     */
    public function distribute () {
        
        foreach ($this->_rules as $curRule) {
            if (method_exists($curRule, 'distribute')) {
                $curRule->distribute($this->_amount / sizeof($this->_rules));
            }
        }
    } // End function distribute()
    
    /**
     * Build rule class name.
     *
     * @param string $name File name without extension
     * @return string
     */
    protected function _getRuleClassName ($name) {
    
        $result = null;
    
        $result = 'LookBetBankRule' . ucfirst($name);
    
        return $result;
    } // End function _getRuleClassName()
    
    /**
     * Load all applicable rules.
     * 
     * @param array $conditions The set of conditions to pick up applicable rules
     * @return false on error
     */
    public function loadRules () {
            
        jimport('joomla.filesystem.folder');
        jimport('joomla.filesystem.file');
        
        $files = JFolder::files($this->_rulesPath . '/', '.*\.php$');
        foreach ($files as $curFile) {
            $curName = JFile::stripExt($curFile);
            $curClass = $this->_getRuleClassName($curName);
            JLoader::import($curName, $this->_rulesPath);
            if ($curName != '' and class_exists($curClass)) {
                $curRule = call_user_func(array($curClass, 'getInstance'),
                                          $this->_conditions);
                if ($curRule !== false) {
                    $this->_rules[] = clone $curRule;
                }
            }
        }

        return true;
    } // End function loadRules()
    
    /**
     * Return LookBetChallenges new class instance for given group.
     *
     * @param decimal $amount Bank amount
     * @param array $conditions The set of conditions to pick up applicable rules
     * @return LookBetChallenges
     */
    static function & getInstance ($amount, $conditions = null) {
        
        $result = new self($amount, $conditions);
        
        return $result;
    }
}