<?php
/**
 * LookBet parameters.
 *
 * @package    LookBet
 * @subpackage Classes
 * @version    3.01.0010 $Id: params.php 109 2014-11-28 20:26:34Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 09-Jun-2014
 * @license    GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.registry');

class LookBetParams extends JRegistry {
    
    protected $_default_min_questions = 2;
    protected $_default_max_questions = 10;
    protected $_default_default_num_questions = 2;
    protected $_default_thumb_width = 90;
    protected $_default_thumb_height = 60;
    protected $_default_results_verification_period = 1; // Number of days before closing contract
    protected $_default_contract_show_questions = 0; // Questions display mode: 0 - show only when vote can
                                                     // be accepted; or 1 - always show
    protected $_default_toplist_size = 10;
    protected $_default_challenge_payment = 25;
    protected $_default_default_contract_price = 1;
    protected $_default_contract_fee = 15; // In per cent
    protected $_default_house_account = '$lookbet'; // Site owner account name

    
    function __construct () {
        
        parent::__construct();
        
        $this->set('min_questions', $this->_default_min_questions);
        $this->set('max_questions', $this->_default_max_questions);
        $this->set('default_num_questions', $this->_default_default_num_questions);
        $this->set('thumb_width', $this->_default_thumb_width);
        $this->set('thumb_height', $this->_default_thumb_height);
        $this->set('results_verification_period', $this->_default_results_verification_period);
        $this->set('contract_show_questions', $this->_default_contract_show_questions);
        $this->set('toplist_size', $this->_default_toplist_size);
        $this->set('challenge_payment', $this->_default_challenge_payment);
        $this->set('default_contract_price', $this->_default_default_contract_price);
        $this->set('contract_fee', $this->_default_contract_fee);
        $this->set('house_account', $this->_default_house_account);
    }
    
    public static function getInstance ($id = null) {
        
        static $result;
        
        if (!isset($result)) {
            $result = new self();
        }
        
        return $result;
    }
}