<?php
/**
 * LookBet Challenges object.
 *
 * @example     JLoader::import('components.com_lookbet.classes.challenges', JPATH_ADMINISTRATOR);
 *              $api = LookBetChallenges::getInstance($idGroup);
 *              
 * @package     LookBet
 * @subpackage  Classes
 * @version     3.02.0007 $Id: challenges.php 119 2014-12-09 00:48:45Z dw.ilya $
 * @copyright   2014
 * @author      Created on 08-Oct-2014
 * @license     GNU/GPL
 * @since       2.00
 */
defined('_JEXEC') or die('Restricted access');

class LookBetChallenges {
    
    protected $_items = null;
    protected $_idGroup = null;
    protected $_error = null;
    
    function __construct ($idGroup, $showAll = false) {

        $this->load($idGroup, $showAll);
    }
    
    /**
     * Checks if given challenge is completed by user.
     *
     * @param integer $id Challenge ID
     * @param integer $idUser User ID. Active user by default
     * @return boolean
     */
    public function isComplete ($id, $idUser = null) {
    
        $result = false;
    
        if (!$idUser) {
            $idUser = JFactory::getUser()->get('id');
        }
        
        if ($id > 0 and $idUser > 0) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $user = JFactory::getUser();
            
            $query
                ->select('*')
                ->from('#__lookbet_user_challenges')
                ->where('id_user = ' . $idUser)
                ->where('id_challenge = ' . $id);
            $db->setQuery($query);
            $result = (boolean)(sizeof($db->loadAssocList()) > 0);
        }
        
        return $result;
    } // End function isComplete()
    
    /**
     * Get error message
     * 
     * @return string
     */
    public function getError () {
    
        $result = $this->_error;
    
        return $result;
    } // End function getError()
    
    /**
     * Set error message.
     *
     * @param string $message Error message text
     * @return string
     */
    public function setError ($message) {
    
        $result = $this->_error = $message;
    
        return $result;
    } // End function setError()
    
    /**
     * Get database table object.
     *
     * @param string $tableName Table name
     * @return JTable
     */
    protected function & _getTable ($tableName) {
    
        static $result = array();
        
        if (!isset($result[$tableName])) {
            $result[$tableName] = JTable::getInstance($tableName, 'Table');
        }
    
        return $result[$tableName];
    } // End function _getTable()
    
    /**
     * Get the group LookBet-specific properties table object.
     *
     * @param boolean   $autoload Load group data
     * @param integer   $idGroup Group ID to load. If ommitted, load current
     *                  group data
     * @return TableGroupProperties or false on error
     */
    protected function & _getGroupPropertiesTable ($autoload = false,
                                                   $idGroup = null) {
    
        $result = null;
    
        $result = $this->_getTable('GroupProperties');

        if ($autoload) {
            if (is_null($idGroup)) {
                $idGroup = $this->_idGroup;
                if (!$idGroup) {
                    $this->setError('Cannot load data. Group ID not set');
                    return false;
                }
            }
            if (!$result->load($idGroup)) {
                $this->setError('Cannot load data');
                return false;
            }
        }
    
        return $result;
    } // End function _getGroupPropertiesTable()
    
    /**
     * Get challenges table.
     * 
     * @return TableChallenges
     */
    public function & getChallengesTable () {
    
        static $result;
        
        if (!isset($result)) {
            $tableName = "Challenges";
            $result = JTable::getInstance($tableName, 'Table');
        }
    
        return $result;
    } // End function getChallengesTable()
    
    /**
     * Delete challenge.
     *
     * @param integer $id Challenge ID
     * @return false on error
     */
    public function delete ($id) {
    
        $result = null;
        
        $db = JFactory::getDbo();
    
        $item =& $this->_items[$id];
        
        $row = $this->getTypeTable($item->challenge_type);
        $challenge = $this->getChallengesTable();
        
        $result = $row->delete($item->id_object) && $challenge->delete($id);
    
        return $result;
    } // End function delete()
    
    /**
     * Add challenge
     *
     * @param string $type Challenge type
     * @param array $data Raw form data
     * @return boolean false on error
     */
    public function add ($type, $data) {
 
        $app = JFactory::getApplication();
        $jinput = $app->input;
        
        
    
        $row = $this->getTypeTable($type);
        $challenge = $this->getChallengesTable();
        
        // Map type-specific data
        $values = array();
        switch ($type) {
            case 'visit':
                $values['link'] = $data['challenge_link'];
            		$values['link_type'] = $data['challenge_link_type'];
                break;
            default:
                // @todo ERROR: Unknown challenge type
                return false;
                break;
        }
        
        // Store type-specific data
        $row->bind($values);

        if (!$row->store()) {
            // @todo ERROR: Cannot save challenge type-specific data
            return false;
        }
        
        // Store generic data
        $challenge->set('id_group', $this->_idGroup);
        $challenge->set('id_object', $row->get('id'));
        $challenge->set('challenge_type', $type);
        if (!$challenge->store()) {
            // @todo ERROR: Cannot save challenge generic data
            $row->delete();
            return false;
        }
    
        return true;
    } // End function add()
    
    /**
     * Get challenges data items.
     * 
     * @return array
     */
    public function getItems () {
    
        $result = null;
    
        $result = $this->_items;
    
        return $result;
    } // End function getItems()
    
    /**
     * Get the table object for challenge type.
     *
     * @param string $type Challenge type
     * @return JTable or false on error
     */
    public function & getTypeTable ($type) {
    
        static $result = array();
        
        if (!isset($result[$type])) {
            $tableName = "Challenge{$type}s";
            $result[$type] = JTable::getInstance($tableName, 'Table');
        }
    
        return $result[$type];
    } // End function getTypeTable()
    
    /**
     * Get data item with type-specific data loaded.
     *
     * @param integer $id Challenge ID
     * @return Object
     */
    public function getTypeData ($id) {
    
        $result = null;
        
        $db = JFactory::getDbo();
    
        $item =& $this->_items[$id];
        
        $row = $this->getTypeTable($item->challenge_type);
        $row->load($item->id_object);

        $properties = $row->getProperties(true);
        
        // Copy all scalar properties except primary key into data item
        foreach ($properties as $curKey => $curValue) {
            if ($curKey != 'id' and !is_object($curValue)) {
                $item->$curKey = $curValue;
            }
        }
        
        $this->_items[$id] = $item;
        $result =& $this->_items[$id];
    
        return $result;
    } // End function getTypeData()
    
    /**
     * Load challenges for given group.
     *
     * @param integer $idGroup Group ID
     * @param boolean $showAll True to include unavailable items
     * @return array of challenge data objects or false on error
     */
    public function load ($idGroup, $showAll = false) {
    
        $result = null;
        
        JLoader::import('classes.params', LOOKBET_PATH_COMPONENT_ADMINISTRATOR);
        $param = LookBetParams::getInstance();
        
        $group = $this->_getGroupPropertiesTable(true, $idGroup);
    
        if ($group->points >= $param->get('challenge_payment')
            or $showAll) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            
            $query
                ->select('*')
                ->from('#__lookbet_challenges')
                ->where('id_group = ' . $idGroup)
                ->order('id DESC');
            $db->setQuery($query);
            $result = $this->_items = $db->loadObjectList('id');
            $this->_idGroup = $idGroup;
        } else {
            $result = $this->_items = array();
        }
    
        return $result;
    } // End function load()
    
    /**
     * Return LookBetChallenges class instance for given group only creating it if not already exists.
     *
     * @param integer $idGroup Group ID
     * @return LookBetChallenges
     */
    static function & getInstance ($idGroup, $showAll = false) {
        
        static $result = array();
        
        if (!isset($result[$idGroup])) {
            $result[$idGroup] = new self($idGroup, $showAll);
        }
        
        return $result[$idGroup];
    }
}