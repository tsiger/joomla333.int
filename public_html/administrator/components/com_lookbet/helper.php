<?php
/**
 * @package    LookBet
 * @subpackage Helpers
 * @version    1.00.0007 $Id: helper.php 87 2014-08-10 19:07:42Z dw.ilya $
 * @since      1.00
 * @license    GNU/GPL
 */

//-- No direct access
defined('_JEXEC') || die('=;)');


/**
 * LookBet helper functions.
 *
 * @package    LookBet
 * @subpackage Helpers
 */
class LookBetHelper {
    
    /**
     * Get contract owner ID.
     *
     * @param integer $idContract Contract ID
     * @return integer
     */
    public static function getContractOwner ($idContract) {
    
        $result = null;
    
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        
        $query
            ->select($db->qn('id_owner'))
            ->from($db->qn('#__lookbet_contracts'))
            ->where($db->qn('id') . " = $idContract");
        $db->setQuery($query);
        $result = $db->loadResult();
    
        return $result;
    } // End function getContractOwner()
    
    /**
     * Get image ID for the vote of given user in a given contract
     * 
     * @param integer $idUser User ID
     * @param integer $idContract Contract ID
     * @return integer
     */
    public static function getUserContractImage ($idUser, $idContract) {
    
        static $result = array();
    
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        
        if (!isset($result[$idUser][$idContract])) {
            // SELECT v.id FROM #__lookbet_votes v LEFT JOIN #__lookbet_questions q ON v.id_question = q.id WHERE v.id_user = $idUser AND q.id_contract = $idContract AND q.has_image ORDER BY q.event_date DESC LIMIT 1
            $query->select('v.id')
                ->from('#__lookbet_votes AS v')
                ->join('LEFT', '#__lookbet_questions AS q ON v.id_question = q.id')
                ->where("v.id_user = $idUser")
                ->where("q.id_contract = $idContract")
                ->where('q.has_image')
                ->order('q.event_date DESC')
                ->setLimit(1);
            $db->setQuery($query);
            $result[$idUser][$idContract] = $db->loadResult();
        }
    
        return $result[$idUser][$idContract];
    } // End function getUserContractImage()
    
    /**
     * Get the number of results set for given contract
     *
     * @param integer $idContract Contract ID
     * @return integer
     */
    public static function getContractResultsCount ($idContract) {
    
        static $result = array();
    
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        
        if (!isset($result[$idContract])) {
            $query
                ->select('COUNT(*)')
                ->from('#__lookbet_questions')
                ->where("id_contract = $idContract")
                ->where('event_result IS NOT NULL')
                ->where('event_result <> \'\'');
            $db->setQuery($query);
            $result[$idContract] = $db->loadResult();
        }
    
        return $result[$idContract];
    } // End function getContractResultsCount()
    
    /**
     * Get user score for a given contract
     * 
     * @param integer $idUser User ID
     * @param integer $idContract Contract ID
     * @return integer
     */
    public static function getUserScore ($idUser, $idContract) {
    
        $result = null;
    
        $result = self::getUserStandings($idUser, $idContract)->score;
    
        return $result;
    } // End function getUserScore()
    
    /**
     * Get user position for a given contract
     * 
     * @param integer $idUser User ID
     * @param integer $idContract Contract ID
     * @return integer
     */
    public static function getUserPosition ($idUser, $idContract) {
    
        $result = null;
    
        $result = self::getUserStandings($idUser, $idContract)->pos;
    
        return $result;
    } // End function getUserPosition()
    
    /**
     * Get positions for all users for a given contract
     * 
     * @param integer $idContract Contract ID
     * @param boolean $leadersOnly If true, limit the list 
     * @return array
     */
    public static function getStandings ($idContract, $leadersOnly = false) {
    
        static $result = array();
        
        if ($leadersOnly or !isset($result[$idContract])) {
            $db = JFactory::getDbo();
            
            // Inintialise database variables
            $db->setQuery('SET @pos := 0');
            $db->execute();
            $db->setQuery('SET @prev_score := NULL');
            $db->execute();
            
            // Build query
            // @todo Query as string! Possible compatibility issue!
            $query = "SELECT pos, id_user, score FROM (SELECT s.*, IF(@prev_score <> score OR @prev_score IS NULL, @pos := @pos + 1, @pos) AS pos, @prev_score := score FROM (SELECT id_user, user_name, SUM(score) score FROM (SELECT `v`.`id_user`,`v`.`forecast`,`q`.`id`,`q`.`event_result`,`u`.`name` `user_name`, IF(`v`.`forecast` = `q`.`event_result`, 1, 0) score FROM `#__lookbet_votes` AS `v` INNER JOIN `#__lookbet_questions` AS `q` ON (`v`.`id_question` = `q`.`id`) LEFT JOIN `#__users` AS `u` ON (`v`.`id_user` = `u`.`id`) WHERE `q`.`id_contract` = '$idContract') `r` GROUP BY `id_user` ORDER BY `score` DESC) `s`) `us`";
            if ($leadersOnly) {
                $query .= ' HAVING `pos` = \'1\'';
            }
            $db->setQuery($query);
            if (!$leadersOnly) {
                // For full list, store in cache
                $result[$idContract] = $db->loadObjectList();
            } else {
                // For leaders list, return directly
                return $db->loadObjectList();
            }
        }
        
        // Return from cache
        return $result[$idContract];
    } // End function getStandings()
    
    /**
     * Get user standings for a given contract
     * 
     * @param integer $idUser User ID
     * @param integer $idContract Contract ID
     * @return array
     */
    public static function getUserStandings ($idUser, $idContract) {
    
        static $result = array();
        
        if (!isset($result[$idUser][$idContract])) {
            $db = JFactory::getDbo();
            
            // Inintialise database variables
            $db->setQuery('SET @pos := 0');
            $db->execute();
            $db->setQuery('SET @prev_score := NULL');
            $db->execute();
            
            // Build query
            // @todo Query as string! Possible compatibility issue!
            $query = "SELECT pos, id_user, score FROM (SELECT s.*, IF(@prev_score <> score OR @prev_score IS NULL, @pos := @pos + 1, @pos) AS pos, @prev_score := score FROM (SELECT id_user, user_name, SUM(score) score FROM (SELECT `v`.`id_user`,`v`.`forecast`,`q`.`id`,`q`.`event_result`,`u`.`name` `user_name`, IF(`v`.`forecast` = `q`.`event_result`, 1, 0) score FROM `#__lookbet_votes` AS `v` INNER JOIN `#__lookbet_questions` AS `q` ON (`v`.`id_question` = `q`.`id`) LEFT JOIN `#__users` AS `u` ON (`v`.`id_user` = `u`.`id`) WHERE `q`.`id_contract` = '$idContract') `r` GROUP BY `id_user` ORDER BY `score` DESC) `s`) `us` HAVING `id_user` = '$idUser'";
            $db->setQuery($query);
            $result[$idUser][$idContract] = $db->loadObject();
        }
    
        return $result[$idUser][$idContract];
    } // End function getUserStandings()
    
    /**
     * Get contract ID for given question.
     *
     * @param integer $questionId Question ID
     * @return integer
     */
    public static function getQuestionContractId ($questionId) {
    
        $result = null;
    
        $db = JFactory::getDbo();
        
        $row = JTable::getInstance('Questions', 'Table');
        
        if ($row->load($questionId)) {
            $result = $row->id_contract;
        }
    
        return $result;
    } // End function getQuestionContractId()
    
    /**
     * Get the date of the last event in contract.
     *
     * @param integer $idContract Contract ID
     * @return string
     */
    public static function getLastEventDate ($idContract) {
    
        $result = null;
    
        JLoader::import('helpers.query', JPATH_COMPONENT_ADMINISTRATOR);
        
        $db = JFactory::getDbo();
        
        $query = LookBetQueryHelper::selectQuestions($idContract, 'MAX(event_date) AS min_event_date');
        $query
            ->order('id')
            ->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadResult();
    
        return $result;
    } // End function getLastEventDate()
    
    /**
     * Get the date of the first event in contract.
     * 
     * @param integer $idContract Contract ID
     * @return string
     */
    public static function getFirstEventDate ($idContract) {
    
        $result = null;
    
        JLoader::import('helpers.query', JPATH_COMPONENT_ADMINISTRATOR);
        
        $db = JFactory::getDbo();
        
        $query = LookBetQueryHelper::selectQuestions($idContract, 'MIN(event_date) AS min_event_date');
        $query
            ->order('id DESC')
            ->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadResult();
    
        return $result;
    } // End function getFirstEventDate()

    /**
     * Checks if the votes for given contract are accepted
     *
     * @todo Add timezone processing
     * @param integer $idContract Contract ID
     * @return boolean
     */
    public static function acceptVotes ($idContract) {
    
        $result = null;
        
        $db = JFactory::getDbo();
        
        $query = $db->getQuery(true);
        
        $query->select(array('IF(MIN(event_date) > NOW(), 1, 0) AS ' . 
                             $db->quoteName('accept_votes')))
            ->from('#__lookbet_questions')
            ->where($db->quoteName('id_contract') . ' = '
                    . $db->quote($idContract, false))
            ->group($db->quoteName('id_contract'));
    
        //$queryString = "SELECT id_contract, IF(MIN(event_date) > NOW(), 1, 0) accept_votes FROM #__lookbet_questions WHERE id_contract = '$idContract' GROUP BY id_contract";
        
        $db->setQuery($query);
        $result = $db->loadResult();
    
        return $result;
    } // End function acceptVotes()
    
    /**
     * Get the forecast type code:
     * - 1 - first participant
     * - 2 - second participant
     * - X - draw
     * - null - forecast code does not match any of participants
     *
     * @param string $forecast forecast code string
     * @param integer $idQuestion question ID number
     *
     * @return string
     */
    public static function getForecastType ($forecast, $idQuestion) {
    
        $result = null;
        
        $question = JTable::getInstance('Questions', 'Table');
        $question->load($idQuestion);
        
        switch (strtolower($forecast)) {
            case strtolower($question->part1):
                $result = '1';
                break;
            case strtolower($question->part2);
                $result = '2';
                break;
            case 'x':
                $result = 'X';
                break;
            default:
                $result = null;
                break;
        }
    
        return $result;
    } // End function getForecastType()
    
    /**
     * Get the number of users who already has voted for given contract.
     * 
     * @return integer
     */
    public static function getContractVotesCount ($idContract) {
    
        static $counts = array();
        
        if (!isset($counts[$idContract])) {
            $db = JFactory::getDbo();
        
            $query = "SELECT COUNT(*) num_votes FROM (SELECT id_contract, id_user FROM #__lookbet_votes nvv LEFT JOIN #__lookbet_questions nvq ON nvv.id_question = nvq.id GROUP BY id_contract, id_user) nv WHERE id_contract = '$idContract' GROUP BY id_contract";
            $db->setQuery($query);
            $counts[$idContract] = $db->loadResult();
            
            if (is_null($counts[$idContract])) {
                $counts[$idContract] = 0;
            }
        }
    
        return $counts[$idContract];
    } // End function getContractVotesCount()
    
    /**
     * Checks if given user has voted for a given conrtact.
     *
     * @param integer $idUser user ID
     * @param integer $idContract contract ID
     * @return boolean
     */
    public static function hasVoted ($idUser, $idContract) {
    
        $result = null;
    
        $db = JFactory::getDbo();
        
        $query = "SELECT COUNT(*) FROM #__lookbet_votes v LEFT JOIN #__lookbet_questions q ON v.id_question = q.id WHERE v.id_user = '$idUser' AND q.id_contract = '$idContract'";
        $db->setQuery($query);
        $cnt = $db->loadResult();
        
        if ($cnt > 0) {
            $result = true;
        } else {
            $result = false;
        }
    
        return $result;
    } // End function hasVoted()
}
