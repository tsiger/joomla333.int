<?php
/**
 * CHANGELOG
 *
 * This is the changelog for LookBet.<br>
 * <b>Please</b> be patient =;)
 *
 * @version    SVN $Id: CHANGELOG.php 21 2014-04-30 18:12:25Z dw.ilya $
 * @package    LookBet
 * @subpackage Documentation
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @author     Created on 19-Apr-2014
 */

//--No direct access to this changelog...
defined('_JEXEC') || die('=;)');

//--For phpDocumentor documentation we need to construct a function ;)
/**
 * CHANGELOG
 * {@source}
 */
function CHANGELOG()
{
/*
_______________________________________________
_______________________________________________

This is the changelog for LookBet

Please be patient =;)
_______________________________________________
_______________________________________________

Legend:

 * -> Security Fix
 # -> Bug Fix
 + -> Addition
 ^ -> Change
 - -> Removed
 ! -> Note
______________________________________________

19-Apr-2014 Factory.DocWriter.Ru
 ! Startup

*/
}//--This is the END
