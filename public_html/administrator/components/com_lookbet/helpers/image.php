<?php
/**
 * LookBet image processing functions
 *
 * @package    LookBet
 * @subpackage Views
 * @version    1.00.0003 $Id: image.php 74 2014-06-23 11:27:08Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 04-Jun-2014
 * @license    GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.file');

class LookBetImageHelper {

    const THUMB_HEIGHT = 60; // Default thumbnail height
    const THUMB_WIDTH = 90; // Default thumbnail width
    
    /**
     * Get gallery image URI.
     *
     * @param string $name Image file name without path and extension
     * @return string
     */
    public static function getImage ($name, $path = false) {
    
        $result = null;
        $imagePath = "images/lookbet/gallery/$name.jpg";

        if(JFile::exists($fullPath = JPATH_ROOT .'/'. $imagePath))
            $result = $path ? $fullPath : JUri::base() . $imagePath;

        return $result;
    } // End function getImage()
    
    /**
     * Get image thumbnale URI creating it if not already exists.
     * 
     * @param string $name Image file name without path and extension
     * @return string
     */
    public static function getThumb ($name) {
    
        $result = null;

        $thumbPath = "images/lookbet/gallery/thumbs/$name.jpg";

        if(JFile::exists(JPATH_ROOT .'/'. $thumbPath)){
            $result = JUri::base() . $thumbPath;
        }
        elseif($fullSizeImage = self::getImage($name, true)){

            JLoader::import('classes.common.image', JPATH_COMPONENT_ADMINISTRATOR);

            $DWImage = new DWImage();

            $DWImage->load($fullSizeImage);
            $DWImage->resize(self::THUMB_WIDTH, self::THUMB_HEIGHT);
            $DWImage->save(JPATH_ROOT .'/'. $thumbPath);

            $result = JUri::base() . $thumbPath;
        }

        return $result;
    } // End function getThumb()
}