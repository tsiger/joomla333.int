<?php
/**
 * LookBet billing integration
 *
 * @package    LookBet
 * @subpackage Views
 * @version    3.00.0002 $Id: billing.php 106 2014-11-13 23:16:57Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 11-Nov-2014
 * @license    GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');

JLoader::import('components.com_billing.useraccount', JPATH_SITE);
JLoader::import('components.com_billing.account', JPATH_SITE);

class LookBetBillingHelper {
    
    private static $_component = 'com_lookbet';

    
    /**
     * Add money to user's account.
     * 
     * @return void
     */
    public static function payToUser ($idUser, $value, $description = '') {
    
        $result = null;
    
        $account = self::getUserAccount();
        
        $account->AddMoney($idUser, $value, date('His') . $idUser, $description);
    
        return $result;
    } // End function payToUser()
    
    /**
     * Get billing user account object.
     * 
     * @return UserAccount
     */
    public static function getUserAccount () {
        
        static $result;
        
        if (!isset($result)) {
            $result = new UserAccount();
        }
    
        return $result;
    } // End function getUserAccount()
    
    /**
     * Get default currency name.
     * 
     * @return string
     */
    public static function getDefaultCurrency () {
    
        static $result;
    
        if (!isset($result)) {
            $result = GetDefaultCurrencyAbbr();
        }
    
        return $result;
    } // End function getDefaultCurrency()
    
    /**
     * Get current user account balance.
     * 
     * @return numeric
     */
    public static function getUserBalance () {
    
        $result = null;
        
        $user = JFactory::getUser();
    
        $result = self::getUserAccount()->GetBalance($user->id);
    
        return $result;
    } // End function getUserBalance()
}