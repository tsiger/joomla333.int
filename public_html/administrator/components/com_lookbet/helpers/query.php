<?php
/**
 * LookBet common query building functions.
 *
 * @package    LookBet
 * @subpackage Views
 * @version    1.00.0002 $Id: query.php 74 2014-06-23 11:27:08Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 20-Jun-2014
 * @license    GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');

class LookBetQueryHelper {
    
    /**
     * Build select query for contract questions.
     *
     * @param integer $idContract contract ID
     * @return JDatabaseQuery
     */
    public static function & selectQuestions ($idContract, $columns = '*') {
    
        static $result;
    
        $db = JFactory::getDbo();
        
        $result = $db->getQuery(true);
        
        $result
            ->select($columns)
            ->from('#__lookbet_questions')
            ->where($db->quoteName('id_contract') . ' = '
                    . $db->quote($idContract))
            ->order(array('event_date', 'id'));
    
        return $result;
    } // End function selectEvents()
}