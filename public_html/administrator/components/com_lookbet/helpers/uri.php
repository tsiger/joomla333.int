<?php
/**
 * LookBet base URI's
 *
 * @package    LookBet
 * @subpackage Views
 * @version    1.00.0004 $Id: uri.php 66 2014-06-10 11:27:21Z dw.ilya $
 * @author     Factory.DocWriter.Ru {@link http://factory.docwriter.ru/}
 * @copyright  2014 Factory.DocWriter.Ru
 * @author     Created on 16-May-2014
 * @license    GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');

class LookBetUriHelper {
    
    private static $_component = 'com_lookbet';
    
    
    /**
     * Get results form URI string.
     *
     * @return string
     */
    public static function saveVote () {
    
        $result = null;
        
        $path = self::_buildUri(null, 'contracts', 'savevote');
        $result = self::_buildRoute($path);
    
        return $result;
    } // End function saveVote()

    /**
     * Build route from relative path.
     * 
     * @return string
     */
    private static function _buildRoute ($path) {
    
        $result = JRoute::_(JURI::root() . $path);
    
        return $result;
    } // End function _buildRoute()
    
    /**
     * Build URI from components.
     *
     * @param string $component component name
     * @param string $controller controller name
     * @param string $task task name
     * @param array|string $attribs extra URI variables
     * @return string
     */
    private static function _buildUri ($component = null, $controller = null,
                                       $task = null, $attribs = array()) {
    
        $result = null;
        
        if (is_null($component)) {
            $component = self::$_component;
        }
        
        $path = "index.php?option=$component";
    
        $uri = JURI::getInstance($path);
        
        $taskParts = array();
        if (!is_null($controller)) {
            $taskParts[] = $controller;
        }
        if (!is_null($task)) {
            $taskParts[] = $task;
        }
        $taskFull = implode('.', $taskParts);
        if ($taskFull != '') {
            $uri->setVar('task', $taskFull);
        }
        foreach ($attribs as $curName => $curValue) {
            $uri->setVar($curName, $curValue);
        }
        
        $result = $uri->toString();
    
        return $result;
    } // End function _buildUri()
    
    /**
     * Get results form URI string.
     *
     * @param integer $idContract contract ID
     * @return string
     */
    public static function results ($idContract) {
    
        $result = null;
        
        $path = self::_buildUri(null, 'contracts', 'results',
                                array('id_contract' => $idContract));
        $result = self::_buildRoute($path);
    
        return $result;
    } // End function results()
    
    /**
     * Get votes list page URI string.
     *
     * @param integer $idContract contract ID
     * @return string
     */
    public static function votes ($idContract) {
    
        $result = null;
        
        $path = self::_buildUri(null, 'contracts', 'votes',
                                array('id_contract' => $idContract));
        $result = self::_buildRoute($path);
    
        return $result;
    } // End function votes()
    
    /**
     * Get new vote form URI string.
     * 
     * @return string
     */
    public static function addVote () {
    
        $result = null;
        
        $path = self::_buildUri(null, 'votes', 'add');
        $result = self::_buildRoute($path);
    
        return $result;
    } // End function addVote()
    
    /**
     * Get new question form URI string.
     * 
     * @return string
     */
    public static function addQuestion () {
    
        $result = null;
        
        $path = self::_buildUri(null, 'questions', 'add');
        $result = self::_buildRoute($path);
    
        return $result;
    } // End function addQuestion()
    
    
    /**
     * Get event main page URI string.
     * 
     * @return string
     */
    public static function main () {
    
        $result = null;
        
        $path = self::_buildUri(null, 'contracts', 'display');
        $result = self::_buildRoute($path);
    
        return $result;
    } // End function main()
    
}