<?php defined('_JEXEC') or die; ?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>">
<?php
$this->setGenerator('');
$this->base = '';
?>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="/templates/<?php echo $this->template; ?>/js/main.js"></script>
<?php
unset($this->_scripts[JURI::root(true).'/media/jui/js/jquery.min.js']);
unset($this->_scripts[JURI::root(true).'/media/jui/js/jquery-noconflict.js']);
unset($this->_scripts[JURI::root(true).'/media/jui/js/jquery-migrate.min.js']);
unset($this->_scripts[JURI::root(true).'/media/system/js/caption.js']);
if (isset($this->_script['text/javascript']))
{ $this->_script['text/javascript'] = preg_replace('%window\.addEvent\    (\'load\',\s*function\(\)\s*{\s*new\s*JCaption\(\'img.caption\'\);\s*}\);\s*%',     '', $this->_script['text/javascript']);
if (empty($this->_script['text/javascript']))
unset($this->_script['text/javascript']);}
$this->_scripts = array();
?>
<jdoc:include type="head" />
<link rel="stylesheet" type="text/css" href="/templates/<?php echo $this->template; ?>/css/normalize.min.css">
<link rel="stylesheet" type="text/css" href="/templates/<?php echo $this->template; ?>/css/style.css">
<!--[if IE]>
<script src="/templates/<?php echo $this->template; ?>/js/ie.js"></script>
<![endif]-->
</head>
<body>
<div class="cont">
<header class="floatleft">
	<div class="logo floatleft">
		<a href=""><img src="/templates/<?php echo $this->template; ?>/img/logo.png" alt="" /></a>
		<span>Свадебный портал Таджикистана</span>
	</div>
	<jdoc:include type="modules" name="phone" style="xhtml" />
	<div class="login floatright">
		<ul class="log-reg floatleft">
			<li><a href="#!">Вход</a></li>
			<li>|</li>
			<li><a href="#!">Регистрация</a></li>
		</ul>
		<div class="floatright">Выбранные услуги</div>
		<span class="floatleft">Вход через ваш аккаунт</span>
		<ul class="soc-top floatright">
			<li><a href="#!"><img src="/templates/<?php echo $this->template; ?>/img/vk-top.png" alt="" /></a></li>
			<li><a href="#!"><img src="/templates/<?php echo $this->template; ?>/img/fb-top.png" alt="" /></a></li>
			<li><a href="#!"><img src="/templates/<?php echo $this->template; ?>/img/tw-top.png" alt="" /></a></li>
			<li><a href="#!"><img src="/templates/<?php echo $this->template; ?>/img/ok-top.png" alt="" /></a></li>
		</ul>
	</div>
	<jdoc:include type="modules" name="lang" style="xhtml" />
	<div class="search">
		<jdoc:include type="modules" name="cities" style="xhtml" />
		<a href="#!" class="floatleft">Карта сайта</a>
		<jdoc:include type="modules" name="search" style="xhtml" />
	</div>
</header>
<jdoc:include type="modules" name="form" style="xhtml" />
<jdoc:include type="modules" name="slider" style="xhtml" />
<section class="floatleft">
	<div class="list floatleft">
		<h2>Каталог услуг</h2>
		<ul>
		<?php
		$parent_id = 8;
		$db = &JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select($db->quoteName(array('alias','title','params')))
			->from($db->quoteName('#__categories'))
			->where($db->quoteName('parent_id')." = ".$db->quote($parent_id));
		$db->setQuery($query);
		$list = $db->loadObjectList();
		foreach ($list as $row){
		 $params = $row->params;
		 $params = explode('images', $row->params);
		 $params = str_replace('\/','/',$params[1]);
		 $params = substr($params, 0, -2);
		 echo '<li><img class="floatleft" src="images'.$params.'" alt="" /><a href="'.$row->alias.'">'.$row->title.'</a></li>';
		}
		?>
		</ul>
		<img class="h-img" src="/templates/<?php echo $this->template; ?>/img/ikonka_katalog_uslug.png" alt="" />
	</div>
	<div class="news-last floatleft">
		<jdoc:include type="modules" name="news" style="xhtml" />
		<jdoc:include type="modules" name="recent" style="xhtml" />
		<jdoc:include type="modules" name="last" style="xhtml" />
	</div>
	<aside class="floatright">
		<jdoc:include type="modules" name="right-menu" style="xhtml" />
		<jdoc:include type="modules" name="car" style="xhtml" />
		<jdoc:include type="modules" name="vk" style="xhtml" />
		<jdoc:include type="modules" name="right-ad" style="xhtml" />
		<img class="fb floatright" src="templates/<?php echo $this->template; ?>/img/fb.png" alt="" />
		<!--<jdoc:include type="modules" name="fb" style="xhtml" />-->
		<jdoc:include type="modules" name="big-menu" style="xhtml" />
		<jdoc:include type="modules" name="addus" style="xhtml" />
	</aside>
	<article class="floatright">
		<jdoc:include type="component" style="xhtml" />
	</article>
	<jdoc:include type="modules" name="shumo" style="xhtml" />
	<jdoc:include type="modules" name="mesto" style="xhtml" />
	<jdoc:include type="modules" name="video" style="xhtml" />
	<jdoc:include type="modules" name="bottom-slider" style="xhtml" />
</section>

<footer class="floatleft">
	<div class="footer-menu floatleft">
		<jdoc:include type="modules" name="cities" style="xhtml" />
		<jdoc:include type="modules" name="footer-menu" style="xhtml" />
	</div>
	<jdoc:include type="modules" name="share" style="xhtml" />
	<div class="adver-copy floatleft">
		<jdoc:include type="modules" name="adver" style="xhtml" />
		<a href="#!" class="to-top floatleft">наверх</a>
		<jdoc:include type="modules" name="copy" style="xhtml" />
	</div>
	<div class="play-search floatright">
		<jdoc:include type="modules" name="play" style="xhtml" />
		<jdoc:include type="modules" name="search" style="xhtml" />
		<jdoc:include type="modules" name="contacts" style="xhtml" />
		<a href="#!"><img class="counter floatleft" src="/templates/<?php echo $this->template; ?>/img/ya.jpg" alt="" /></a>
		<a href="#!"><img class="counter floatleft" src="/templates/<?php echo $this->template; ?>/img/live.jpg" alt="" /></a>
	</div>
</footer>
</div><!--cont-->
<div class="footer-bg"></div>
<div class="bg"></div>
<div class="callback">
	<jdoc:include type="modules" name="callback" style="xhtml" />
	<div class="close"></div>
</div>
<div class="overlay"></div>
</body>
</html>