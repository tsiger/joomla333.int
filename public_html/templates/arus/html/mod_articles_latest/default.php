<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<?php foreach ($list as $item) :  ?>
	<div class="last-item floatleft">
		<?php if($item->catid == 40) {echo '<div class="otzyv-author magenta">Гость</div>';} ?>
		<a href="<?php echo $item->link; ?>" itemprop="url">
		<?php echo $item->title; ?>
		</a>
		<?php 
		$date = substr($item->created, 0, -9);
		if($item->catid == 40) {echo $item->introtext.'<div class="date floatleft magenta">'.$date.'</div>';} else {
		$text = mb_substr($item->introtext, '0', '100');
		echo $text;
		}
		?>
	</div>
<?php 
endforeach;
$i = 0;
foreach ($list as $item2) {
if($item2->catid == 40) {
 echo '<a href="#!" class="post-otzyv floatright">'.JText::_ ('HOME_POST_OTZYV_LINK').'</a>';
}
if (++$i == 1) break;
}
?> 